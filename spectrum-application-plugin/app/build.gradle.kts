plugins {
    kotlin("jvm") version "1.5.0"
    id("codes.spectrum.application") version "1.0"
}

app {
    // установка этого поля
    mainClassFullName = "codes.spectrum.app.AppKt"
    // превратится в вызов функции `application { mainClass.set("codes.spectrum.app.AppKt") }`

    // экстешен-функция сгенерированная Gradle для плагина org.gradle.java
    java {}
    // аналогична нашему акцессору
    javaPlugin.apply {}

    // экстешен-функция сгенерированная Gradle для плагина org.gradle.application
    application {}
    // аналогична нашему акцессору
    javaApplication.apply {}

    // экстешен-функция сгенерированная Gradle для плагина com.google.cloud.tools.jib
    jib {}
    // аналогична нашему акцессору
    jib.apply {}
}