plugins {
    `kotlin-dsl`
    `maven-publish`
    `java-gradle-plugin`
}

gradlePlugin {
    plugins {
        create("spectrumApplicationPlugin") {
            id = "codes.spectrum.application"
            implementationClass = "codes.spectrum.gradle.application.SpectrumApplicationPlugin"
        }
    }
}

dependencies {
    implementation("gradle.plugin.com.google.cloud.tools:jib-gradle-plugin:3.1.2")
}