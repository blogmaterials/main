package codes.spectrum.gradle.application

import com.google.cloud.tools.jib.gradle.JibExtension
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.JavaApplication
import org.gradle.api.plugins.JavaPluginExtension
import org.gradle.kotlin.dsl.getValue
import org.gradle.kotlin.dsl.provideDelegate
import org.gradle.kotlin.dsl.setValue
import java.net.InetAddress

class SpectrumApplicationPlugin : Plugin<Project> {
    override fun apply(target: Project) {
        target.apply {
            plugin("org.gradle.application")
            plugin("com.google.cloud.tools.jib")
        }
        target.extensions.create("app", SpectrumApplicationExtension::class.java, target)
    }
}

abstract class SpectrumApplicationExtension(private val target: Project) {
    private inline fun <reified T> extension() = target.extensions.getByType(T::class.java)

    /**
     * Так как создание объекта из данного класса происходит ПОСЛЕ применения к проекту плагинов application и jib,
     * то мы можем быть уверены, что в контейнере `target.extensions` уже есть данные экстеншены
     */
    val javaPlugin = extension<JavaPluginExtension>()
    val javaApplication = extension<JavaApplication>()
    val jib = extension<JibExtension>()

    var mainClassFullName: String by javaApplication.mainClass

    private val jibDefault: JibExtension.() -> Unit by lazy {
        {
            from {
                image = "openjdk:11.0.4"
            }
            container {
                creationTime = "USE_CURRENT_TIMESTAMP"
                workingDirectory = "/app"
                environment = mapOf(
                    "BUILD_HOST" to InetAddress.getLocalHost().hostName,
                    "BUILD_USER" to System.getProperty("user.name"),
                    "SERVICE_NAME" to "${target.rootProject.name}-${target.name}",
                    "PROJECT_VERSION" to "${target.version}"
                )
            }
        }
    }

    init {
        mainClassFullName = "MainKt"
        jib.apply(jibDefault)
    }
}

