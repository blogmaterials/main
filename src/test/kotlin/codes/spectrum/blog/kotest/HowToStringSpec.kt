package codes.spectrum.blog.kotest

import io.kotest.assertions.assertSoftly
import io.kotest.assertions.withClue
import io.kotest.core.spec.Spec
import io.kotest.core.spec.style.StringSpec
import io.kotest.core.test.TestCase
import io.kotest.inspectors.forAll
import io.kotest.matchers.ints.shouldBeInRange
import io.kotest.matchers.ints.shouldBeLessThan
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldMatch
import java.time.LocalDate


abstract class MySpec(
    val current_year: Int = LocalDate.now().year,
    val probes: Int = 3,
    val upper_limit: Int = 4
) : StringSpec() {
    fun createBestTest() {
        "Лучший тест $current_year" {
            this.testCase.description.name.displayName shouldBe "Лучший тест $current_year"
        }
    }

    var startCount: Int = 0

    override fun beforeTest(testCase: TestCase) {
        println(testCase.displayName)
        startCount++
    }

    override fun afterSpec(spec: Spec) {
        super.afterSpec(spec)
        println("Запустили $startCount тестов")
    }
}

class HowToStringSpec : MySpec(current_year = 2045, probes = 3, upper_limit = 5) {
    init {

        createBestTest()

        "2 + 2 = 4" {
            (2 + 2) shouldBe 4
        }

        for (i in 1..probes) {
            "Это число : $i меньше $upper_limit" {
                i shouldBeLessThan upper_limit
            }
        }


        "Проверяем нашу структуру"{
            val mystruct = ManyFields("ПетР1", 78)
            assertSoftly {
                withClue("Название") {
                    withClue("только буквы") {
                        mystruct.name shouldMatch """^\p{L}+$""".toRegex()
                    }
                    withClue("первая заглавная, остальные малые") {
                        mystruct.name shouldMatch """^\p{Lu}\p{Ll}+$""".toRegex()
                    }
                }
                withClue("Возраст") {
                    mystruct.age shouldBeInRange 1..110
                }
            }
        }

    }


    data class ManyFields(
        val name: String = "Иван",
        val age: Int = 23
    )

}