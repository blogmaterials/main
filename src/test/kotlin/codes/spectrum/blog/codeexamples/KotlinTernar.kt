package codes.spectrum.blog.codeexamples

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class UsageOfVeryCoolTripleOperator : StringSpec() {
    init {
        "У меня есть тернарный оператор"{
            var x = 10
            val kotlin_ternar = if (x > 4) 100 else -100
            val myspec_ternar = (x > 4)(100)(-100)
            val kotlin_lambda_ternar = if ({ x > 4 }()) ({ 100 }()) else ({ -100 }())
            val myspec_lambda_ternar = ({ x > 4 }{ 100 }){ -100 }
            myspec_ternar shouldBe kotlin_ternar
            myspec_lambda_ternar shouldBe kotlin_lambda_ternar
        }

        val rus = "[А-ЯЁа-яё]".toRegex()
        "Обычное использование"{
            fun String.lang(): String = (contains(rus))("rus")("eng")
            "dog".lang() shouldBe "eng"
            "собака".lang() shouldBe "rus"
        }
        "Поддерживает ленивые вычисления"{
            fun String.langLazy(): String = ({ contains(rus) }{ "rus" }){ "eng" }
            "dog".langLazy() shouldBe "eng"
            "собака".langLazy() shouldBe "rus"
        }

        "Ternar class work"{
            var x = 10
            val ternar = TernarIfThen<Int>({x>4}, 100)
            val result = ternar(-100)
            result shouldBe 100
            x = 3
            TernarIfThen<Int>({x>4}, 100)(-100) shouldBe -100

            val myspec_ternar = (x > 4)(100)(-100)
            val myspec_ternar_demistified = (x > 4).invoke(100).invoke(-100)
            val myspec_ternar_demistified2 = TernarIfThen((x > 4),(100)).invoke(-100)
        }
    }
}

// определим все преобразования с учетом как ленивых, так и не ленивых boolean
class TernarIfThen<R> private constructor(
    private val ifval: Boolean? = null,
    private val iffun: (() -> Boolean)? = null,
    private val thenval: R? = null,
    private val thenfun: (() -> R)? = null
) {
    constructor(ifval: Boolean, thenval: R) : this(ifval = ifval, iffun = null, thenval = thenval, thenfun = null)
    constructor(ifval: Boolean, thenfun: () -> R) : this(ifval = ifval, iffun = null, thenval = null, thenfun = thenfun)
    constructor(iffun: () -> Boolean, thenval: R) : this(ifval = null, iffun = iffun, thenval = thenval, thenfun = null)
    constructor(iffun: () -> Boolean, thenfun: () -> R) : this(
        ifval = null,
        iffun = iffun,
        thenval = null,
        thenfun = thenfun
    )

    private fun ifer(): Boolean = ifval ?: iffun!!.invoke()
    private fun thener(): R = thenval ?: thenfun!!.invoke()
    operator fun invoke(elser: () -> R) = if (ifer()) thener() else elser()
    operator fun invoke(elser: R) = this.invoke { elser }
}

operator fun <R> Boolean.invoke(then: R) = TernarIfThen(this, then)
operator fun <R> (() -> Boolean).invoke(then: R) = TernarIfThen(this, then)
operator fun <R> Boolean.invoke(then: () -> R) = TernarIfThen(this, then)
operator fun <R> (() -> Boolean).invoke(then: () -> R) = TernarIfThen(this, then)
