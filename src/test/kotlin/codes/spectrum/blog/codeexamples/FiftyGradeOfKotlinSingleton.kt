package codes.spectrum.blog.codeexamples

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeSameInstanceAs

// 50 оттенков синглетона в Kotlin
class FiftyGradeOfKotlinSingleton : StringSpec() {
    /**
     * Допустим у нас есть некий интерфейс
     */
    interface ISomeInterface {
        val someValue: Int
    }

// Далее будем очень по разному реализовывать синглетон, реализующий этот интерфейс

    /**
     * Синглетон "из коробки" - ключевое слово `object`
     */
    object SomeInterfaceObject : ISomeInterface {
        override val someValue: Int = 5
    }

    init {
        "Работа с обычным синглетоном"{
            // обычный синглетон может использоваться как объект
            SomeInterfaceObject.someValue shouldBe 5
            // одновременно ссылка на класс и значение
            val some: SomeInterfaceObject = SomeInterfaceObject
            some.someValue shouldBe 5
            val isome: ISomeInterface = SomeInterfaceObject
            isome.someValue shouldBe 5
            // и это реально один и тот же объект
            SomeInterfaceObject shouldBeSameInstanceAs SomeInterfaceObject
        }
    }

    // но нам может хотеться чуть большего, например возможность иметь
    // и синглетон и просто класс

    class SomeWithDefault(override val someValue: Int = 5) : ISomeInterface {
        companion object {
            val Default = SomeWithDefault()
        }
    }

    init {
        "Работа со значением по умолчанию"{
            SomeWithDefault.Default.someValue shouldBe 5
            // но при этом у нас есть и класс
            SomeWithDefault(6).someValue shouldBe 6
        }
    }

    class SomeWithDefaultExtended(override val someValue: Int = 5) : ISomeInterface {
        companion object : ISomeInterface { // упс! компаньоны то тоже могут имлпементировать интерфейсе
            // и действительно - в ленивом варианте лучше! вдруг не понадобится и вообще отсроченная инициализация
            // в общем случае лучше
            val Default by lazy { SomeWithDefaultExtended() }
            // и через делегирование реализуем наш интерфейс
            override val someValue: Int get() = Default.someValue
        }
    }


    init {
        "Работа с компаньоном - дефолтом"{
            // теперь кроме все тех же:
            SomeWithDefaultExtended.Default.someValue shouldBe 5

            // у нас есть просто
            SomeWithDefaultExtended.someValue shouldBe 5

            // как буд-то у нас `object`
            SomeWithDefaultExtended.Companion.someValue shouldBe 5
            val iface : ISomeInterface = SomeWithDefaultExtended.Companion
            // да, к компаньону можно просто обращаться
            iface.someValue shouldBe 5
        }
    }

    // На этой же технике можно получить полное воспроизведение поведения `object`
    // в первую очередь приватный конструктор
    class SomeCustomObject internal constructor(override val someValue: Int) : ISomeInterface {
        companion object : ISomeInterface { // тут все как и раньше
            // а вот Default стал приватом
            internal val default by lazy { SomeCustomObject (5)}
            override val someValue: Int get() = default.someValue
        }
    }
    init {
        "Наш CustomObject почти как изначальный объект"{
            // Нельзя создать самостоятельно и используем как синглетон
            SomeCustomObject.someValue shouldBe 5
            // Но тем не менее у него ЕСТЬ конструктор и его видит идея
            //SomeCustomObject(10) // хотя и подчеркивает красным
            // и у него есть Companion
            SomeCustomObject.Companion.someValue shouldBe 5
        }
    }

    /**
     * А еще мы можем делать псевдоконструкторы и делать например
     * такие замкнутые фабрики, напомню что sealed == abstract
     */
    sealed class SomeSealed constructor(override val someValue: Int) : ISomeInterface {
        private object SomeSealedDefault: SomeSealed(5)
        private class SomeSealedTwicer(base:Int): SomeSealed(base * 2)
        private class SomeSealedTrippler(base:Int): SomeSealed(base * 3)
        private class SomeSealedOcter(base: Int) : SomeSealed(base * 8)
        companion object : ISomeInterface {
            val Default : SomeSealed = SomeSealedDefault
            override val someValue: Int = SomeSealedDefault.someValue
            operator fun invoke(base: Int): SomeSealed = when {
                base % 2 != 0 -> SomeSealedTwicer(base)
                base % 3 != 0 -> SomeSealedTrippler(base)
                else -> SomeSealedOcter(base)
            }
            fun create(base: Int): SomeSealed = when {
                base % 2 != 0 -> SomeSealedTwicer(base)
                base % 3 != 0 -> SomeSealedTrippler(base)
                else -> SomeSealedOcter(base)
            }
        }
    }

    init {
        "Вот мы работаем с абстракцией так, как будто она не абстрактная"{
            SomeSealed.create(4).someValue shouldBe 12
            SomeSealed.someValue shouldBe 5
            SomeSealed(3).someValue shouldBe 6 // twicer
            SomeSealed(4).someValue shouldBe 12 // trippler
            SomeSealed(6).someValue shouldBe 48 // octer
        }
    }

    // а теперь главный финт - дефолтный ИНТЕРФЕЙС да еще и в 2х вариантаъ

    // Допустим у нас в одной библиотеке написано такой интерфейс
    interface ISuperInterfaceSameLib {
        val superProp: Int
        companion object : ISuperInterfaceSameLib by SuperInterfaceSameLib.Companion
    }

    private class SuperInterfaceSameLib(override val superProp: Int = 5):ISuperInterfaceSameLib {
        companion object : ISuperInterfaceSameLib {
            val Default by lazy { SuperInterfaceSameLib () }
            override val superProp: Int get() = Default.superProp
        }
    }

    init {
        "Все, теперь у нас скрытый класс, а интерфейс при этом имеет дефолтную реализацию"{
            // получился интерфейс, имеющий синглетон
            ISuperInterfaceSameLib.superProp shouldBe 5
        }
    }

    // Но и это не все
    // допустим у нас есть либа с интерфейсом (без внешних зависимостей) и другая либа с нашей реализацией
    // можем ли мы дочернюю либу назначить интерфейсу? - Можем

    interface ISuperInterfaceDifferentLib {
        val superProp: Int
        companion object // именно так ПУСТОЙ компаньон (!!!)
    }
    // в другой либе скрытый от лишних глаз класс
    private class SuperInterfaceDifferentLib(override val superProp: Int = 5):ISuperInterfaceDifferentLib {
        companion object : ISuperInterfaceDifferentLib {
            val Default by lazy { SuperInterfaceDifferentLib () }
            override val superProp: Int get() = Default.superProp
        }
    }
    // и немного хаков на расширениях:
    val ISuperInterfaceDifferentLib.Companion.superProp get() = SuperInterfaceDifferentLib.superProp
    val ISuperInterfaceDifferentLib.Companion.Default : ISuperInterfaceDifferentLib get() =
        SuperInterfaceDifferentLib.Default

    // фабричный метод интернального класса, опубликованный под видом КОНСТРУКТРА ИНТЕРФЕЙСА )))
    operator fun ISuperInterfaceDifferentLib.Companion.invoke(superProp: Int):ISuperInterfaceDifferentLib =
        SuperInterfaceDifferentLib(superProp)

    init {
        "И вот у нас имплементированный в сторонней библиотеке дефолтный синглетон"{
            ISuperInterfaceDifferentLib.superProp shouldBe 5
            ISuperInterfaceDifferentLib.Default.superProp shouldBe 5
            // даже конструктор есть
            ISuperInterfaceDifferentLib(10).superProp shouldBe 10
        }
    }

    // Внимание! Все это очень мощно и может выстрелить в ногу, используйте все эти инструменты только где нужно!
}
