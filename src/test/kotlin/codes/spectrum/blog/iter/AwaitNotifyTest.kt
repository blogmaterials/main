package codes.spectrum.blog.iter

import io.kotest.core.spec.style.StringSpec
import kotlin.concurrent.thread

class AwaitNotifyTest : StringSpec() {
    init {
        "producer - subscriber"{
            var value = 0
            var pending = false
            var finished = false
            val lock = Object()

            val producer = thread {
                synchronized(lock) {
                    println("w:start")
                    lock.wait()
                    repeat(3){
                        pending = true
                        value = it
                        println("w:sending")
                        lock.notify()
                        println("w:wait")
                        lock.wait()
                    }
                    finished = true
                    println("w:finished")
                    lock.notify()
                }
            }
            val subscriber = thread {
                synchronized(lock) {
                    println("r:start")
                    lock.notify()
                    while (true) {
                        println("r:wait")
                        lock.wait()
                        if(pending){
                            println(value)
                            pending = false
                        }
                        println("r:processed")
                        lock.notify()
                        if(finished  ){
                            println("r:finished")
                            break
                        }

                    }
                }
            }
            producer.join()
            subscriber.join()
        }
    }
}