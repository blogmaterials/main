package codes.spectrum.blog.architecture.kissorsolid.geom.kiss


import io.kotest.core.spec.style.StringSpec
import io.kotest.inspectors.forAll
import io.kotest.matchers.shouldBe
import kotlin.math.sqrt

internal class GeoTest : StringSpec() {
    init {
        "Дистанция между точками" {
            data class T(val p1: Point, val p2: Point, val exp: Number) {
                fun test() {
                    p1.ditanceTo(p2) shouldBe exp.toDouble()
                }
            }
            listOf(
                T(Point(0, 0), Point(3, 4), 5),
                T(Point(0, 0, 0), Point(1, 1, 1), sqrt(3.0)),
                T(Point(1, 1, 1), Point(2, 2, 2), sqrt(3.0)),
                T(Point(1.6, 3.1, 1.7), Point(2.2, 1.2, 20), 18.40815036879045),
                T(Point(0, 0, 0), Point(1, 1, 0), sqrt(2.0)),
            ).forAll {
                it.test()
            }
        }

        "Площадь круга"{
            data class T(val r: Number, val exp: Number) {
                fun test() = Circle(r).area() shouldBe exp.toDouble()
            }
            listOf(
                T(1, Math.PI),
                T(2, Math.PI * 4),
                T(3, Math.PI * 9)
            ).forAll { it.test() }
        }

        "Периметр круга"{
            data class T(val r: Number, val exp: Number) {
                fun test() = Circle(r).perimeter() shouldBe exp.toDouble()
            }
            listOf(
                T(1, Math.PI * 2),
                T(2, Math.PI * 4),
                T(3, Math.PI * 6)
            ).forAll { it.test() }
        }

        "Создание круга по точкам"{
            data class T(val p1: Point, val p2: Point) {
                fun test() = Circle.createByPoints(p1, p2).radius shouldBe p1.ditanceTo(p2)
            }
            listOf(
                T(Point(1,2.0), Point(4.3, 2)),
                T(Point(1,2.0, 4.10), Point(4.3, 2, 5))
            ).forAll { it.test() }
        }

    }
}