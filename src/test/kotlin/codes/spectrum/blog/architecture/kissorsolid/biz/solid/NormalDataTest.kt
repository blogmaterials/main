package codes.spectrum.blog.architecture.kissorsolid.biz.solid

import codes.spectrum.blog.architecture.kissorsolid.biz.solid.analytics.NormalDataAnalyzer
import io.kotest.assertions.assertSoftly
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.shouldBe
import java.time.LocalDate

internal class NormalDataTest : StringSpec() {
    init {
        "Проверяем доп поля"{
            NormalData(
                fio = "ИВАНОВ ИВАН ИВАНОВИЧ",
                age = 37,
                actualityDate = LocalDate.parse("2020-05-05"),
                regions = setOf(66,78, 20)
            ).run{
                assertSoftly {
                    with(NormalDataAnalyzer(this)) {
                        isFioDefined.shouldBeTrue()
                        isAgeDefined.shouldBeTrue()
                        isActualityDefined.shouldBeTrue()
                        minBirthYear shouldBe 1982
                        maxBirthYear shouldBe 1983
                    }
                }
            }
        }
    }
}