package codes.spectrum.blog.architecture.kissorsolid.biz.solid

import codes.spectrum.blog.architecture.kissorsolid.biz.solid.adapters.convertTo
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Assertions.*
import java.time.LocalDate
import java.time.LocalDateTime

internal class VendorDataTest : StringSpec() {
    init {
        "Нормализуем типовую вендорную дату"{
            VendorData(
                fio = " Иванов Иван   Иванович",
                age = 37,
                recordDate = LocalDateTime.parse("2020-05-05T15:16:17"),
                region_codes = "  66,  78,  20, 66"
            ).convertTo<NormalData>() shouldBe NormalData(
                fio = "ИВАНОВ ИВАН ИВАНОВИЧ",
                age = 37,
                actualityDate = LocalDate.parse("2020-05-05"),
                regions = setOf(66,78, 20)
            )
        }
    }
}