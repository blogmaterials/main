package codes.spectrum.blog.architecture.kissorsolid.geom.solid

import io.kotest.core.spec.style.StringSpec
import io.kotest.inspectors.forAll
import io.kotest.matchers.shouldBe
import kotlin.math.sqrt

internal class GeoTest : StringSpec() {
    init {
        "Радиус круга для Double"{
            data class T(val p1:Point<Double>,val p2:Point<Double>, val exp: Number) {
                fun test() = Circle(p1, p2).radius shouldBe exp.toDouble()
            }
            listOf(
                T(Point(0.0, 0.0), Point(0.0, 1.0), 1.0),
                T(Point(0.0, 0.0), Point(1.0, 1.0), sqrt(2.0)),
                T(Point(0.0, 0.0), Point(0.0, 2.0), 2.0),
                T(Point(0.0, 0.0), Point(0.0, 3.0), 3.0)
            ).forAll { it.test() }
        }

        "Площадь круга для Double"{
            data class T(val p1:Point<Double>,val p2:Point<Double>, val exp: Number) {
                fun test() = Circle(p1, p2).area() shouldBe exp.toDouble()
            }
            listOf(
                T(Point(0.0, 0.0), Point(0.0, 1.0), Math.PI),
                T(Point(0.0, 0.0), Point(0.0, 2.0), Math.PI * 4),
                T(Point(0.0, 0.0), Point(0.0, 3.0), Math.PI * 9)
            ).forAll { it.test() }
        }

        "Периметр круга"{
            data class T(val p1:Point<Double>,val p2:Point<Double>, val exp: Number) {
                fun test() = Circle(p1, p2).perimeter() shouldBe exp.toDouble()
            }
            listOf(
                T(Point(0.0, 0.0), Point(0.0, 1.0), Math.PI * 2),
                T(Point(0.0, 0.0), Point(0.0, 2.0), Math.PI * 4),
                T(Point(0.0, 0.0), Point(0.0, 3.0), Math.PI * 6)
            ).forAll { it.test() }
        }
    }
}