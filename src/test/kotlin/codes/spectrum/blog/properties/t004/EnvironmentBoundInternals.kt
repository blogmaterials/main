package codes.spectrum.blog.properties.t004

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KClass
import kotlin.reflect.KProperty

abstract class EnvironmentBoundBase(
    protected val env: Map<String, String?> = System.getenv()
) {
    protected val _intenalMap = mutableMapOf<String, Any>()
    protected inline fun <reified T : Any> map(
        vararg probes: String,
        noinline onInitializeHook: () -> Unit = {}
    ): ReadWriteProperty<Any, T> = MutableEnvBoundProperty(
        targetMap = _intenalMap,
        probes = probes.toList(),
        source = env,
        clazz = T::class,
        onInitializeHook = onInitializeHook
    )
    protected val map: ReadWriteProperty<Any, String> get() = map()
    protected val int_map: ReadWriteProperty<Any, Int> get() = map()
    fun toMap(): Map<String, Any> = _intenalMap.toMap()
}


class MutableEnvBoundProperty<RECIEVER, RETTYPE : Any>(
    private val targetMap: MutableMap<String, Any>,
    /**
     * Варианты имен
     */
    private val probes: List<String> = emptyList(),
    private val source: Map<String, String?> = System.getenv(),
    private val clazz: KClass<RETTYPE>,
    private val onInitializeHook: () -> Unit
) : ReadWriteProperty<RECIEVER, RETTYPE> {

    override fun getValue(thisRef: RECIEVER, property: KProperty<*>): RETTYPE {
        return targetMap.computeIfAbsent(property.name) {
            val names = probes.takeIf { it.isNotEmpty() } ?: listOf(property.name)
            val defaultValue: String = getDefault(names)
            val adaptedValue: RETTYPE = adaptValue(defaultValue)
            onInitializeHook()
            adaptedValue
        } as RETTYPE
    }

    override fun setValue(thisRef: RECIEVER, property: KProperty<*>, value: RETTYPE) {
        targetMap[property.name] = value
    }

    private fun getDefault(probes: List<String>): String {
        return probes.map {
            source[it] ?: source[it.toLowerCase()] ?: source[it.toUpperCase()]
        }.filterNotNull().firstOrNull() ?: ""
    }

    private fun adaptValue(value: String?): RETTYPE {
        return when {
            clazz == String::class -> (value ?: "") as RETTYPE
            clazz == Int::class -> (value.takeUnless { it.isNullOrBlank() } ?: "0").toInt() as RETTYPE
            else -> throw Exception("Not supported type")
        }
    }
}
