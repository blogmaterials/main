package codes.spectrum.blog.properties.t004

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.maps.shouldContainKey
import io.kotest.matchers.maps.shouldNotContainKey
import io.kotest.matchers.shouldBe


/**
 * Фаза 4, простое делегированное свойство
 */
class EnvironmentBound(
    env: Map<String, String?> = System.getenv()
) : EnvironmentBoundBase(env) {
    var my_val: String by map()
    var some_other_val by map
    var my_int by int_map
    var renamed_val: Int by map("NEW_NAME", "OLD_NAME") {
        renamed_val_was_called = true
    }
    var renamed_val_was_called: Boolean = false
        private set
}

internal class EnvironmentBoundTest : StringSpec() {
    init {

        "Могу работать через карту" {
            val map = mapOf(
                "MY_VAL" to "X",
                "SOME_OTHER_VAL" to "Y",
                "OLD_NAME" to "4",
                "MY_INT" to "6",
                "X" to "X!"
            )
            val envbound = EnvironmentBound(map)
            envbound.my_val shouldBe "X"
            envbound.my_val = "newval"
            envbound.my_int shouldBe 6
            envbound.toMap()["my_val"] shouldBe "newval"

            envbound.toMap().shouldNotContainKey("old_name") // этой проблемы больше нет

            // эта проблема еще есть
            envbound.toMap().shouldNotContainKey("renamed_val")
            // зато есть новая
            envbound.toMap().shouldNotContainKey("some_other_val")

            envbound.renamed_val_was_called.shouldBeFalse()
            envbound.renamed_val shouldBe 4
            envbound.toMap().shouldContainKey("renamed_val")
            envbound.renamed_val_was_called.shouldBeTrue()

        }

    }
}