package codes.spectrum.blog.properties.t002

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.shouldBe
import io.kotest.mpp.env

/**
 * Фаза 2, сделали просто ленивым
 */
class EnvironmentBound(
    private val env: Map<String, String?> = System.getenv()
) {
    val my_val by lazy { env["MY_VAL"] ?: "" }
    val some_other_val by lazy { env["SOME_OTHER_VAL"] ?: "" }
    val renamed_val by lazy { renamed_val_was_called = true; env["NEW_NAME"] ?: env["OLD_NAME"] ?: "" }
    var renamed_val_was_called: Boolean = false
        private set
}

internal class EnvironmentBoundTest : StringSpec() {
    init {
        "Могу читать свойства"{
            val map = mapOf(
                "MY_VAL" to "X",
                "SOME_OTHER_VAL" to "Y",
                "OLD_NAME" to "Z",
                "X" to "X!"
            )
            val envbound = EnvironmentBound(map)
            envbound.my_val shouldBe "X"
            envbound.some_other_val shouldBe "Y"
            envbound.renamed_val_was_called.shouldBeFalse()
            envbound.renamed_val shouldBe "Z"
            envbound.renamed_val_was_called.shouldBeTrue()
        }
    }
}