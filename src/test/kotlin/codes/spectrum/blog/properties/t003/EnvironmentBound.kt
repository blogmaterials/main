package codes.spectrum.blog.properties.t003

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.maps.shouldContain
import io.kotest.matchers.maps.shouldContainKey
import io.kotest.matchers.maps.shouldNotContain
import io.kotest.matchers.maps.shouldNotContainKey
import io.kotest.matchers.shouldBe
import io.kotest.mpp.env

/**
 * Фаза 3, пытаемся сделать на запись и со словаре и с типизацией
 */
class EnvironmentBound(
    env: Map<String, String?> = System.getenv()
) {
    private val _internalMap =
        env.filterNot { null == it.value }.map { it.key.toLowerCase() to it.value!! }.toMap().toMutableMap()
    var my_val by _internalMap
    var some_other_val by _internalMap
    var renamed_val: Int
        get() {
            renamed_val_was_called = true;
            return _internalMap.computeIfAbsent("renamed_val") {
                _internalMap["new_name"] ?: _internalMap["old_name"] ?: "0"
            }.toInt()
        }
        set(value) {
            _internalMap["new_name"] = value.toString()
        }
    var renamed_val_was_called: Boolean = false
        private set

    fun toMap() = _internalMap.toMap()
}

internal class EnvironmentBoundTest : StringSpec() {
    init {

        "Могу работать через карту" {
            val map = mapOf(
                "MY_VAL" to "X",
                "SOME_OTHER_VAL" to "Y",
                "OLD_NAME" to "4",
                "X" to "X!"
            )
            val envbound = EnvironmentBound(map)
            envbound.my_val shouldBe "X"
            envbound.my_val = "newval"
            envbound.toMap()["my_val"] shouldBe "newval"
            // проблемы
            envbound.toMap().shouldContainKey("old_name")
            envbound.toMap().shouldNotContainKey("renamed_val")

            envbound.renamed_val shouldBe 4
            envbound.toMap().shouldContainKey("renamed_val")

        }
    }
}