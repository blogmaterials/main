package codes.spectrum.blog.properties.t001

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

/**
 * Фаза 1, просто явное чтение
 */
class EnvironmentBound(
    env: Map<String, String?> = System.getenv()
) {
    val my_val = env["MY_VAL"] ?: ""
    val some_other_val = env["SOME_OTHER_VAL"] ?: ""
    val renamed_val = env["NEW_NAME"] ?: env["OLD_NAME"] ?: ""
}

internal class EnvironmentBoundTest : StringSpec() {
    init {
        "Могу читать свойства"{
            val map = mapOf(
                "MY_VAL" to "X",
                "SOME_OTHER_VAL" to "Y",
                "OLD_NAME" to "Z"
            )
            val envbound = EnvironmentBound(map)
            envbound.my_val shouldBe "X"
            envbound.some_other_val shouldBe "Y"
            envbound.renamed_val shouldBe "Z"
        }
    }
}