package codes.spectrum.blog.properties.е005

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KClass
import kotlin.reflect.KProperty

abstract class EnvironmentBoundBase(
    protected val env: Map<String, String?> = System.getenv()
) {
    protected val _intenalMap = mutableMapOf<String, () -> Any>()
    protected inline fun <reified T : Any> map(
        vararg probes: String,
        noinline onInitializeHook: () -> Unit = {}
    ) = MutableEnvBoundPropertyProvider<Any, T>(
        targetMap = _intenalMap,
        probes = probes.toList(),
        source = env,
        clazz = T::class,
        onInitializeHook = onInitializeHook
    )

    protected val map: MutableEnvBoundPropertyProvider<Any, String> get() = map()
    protected val int_map: MutableEnvBoundPropertyProvider<Any, Int> get() = map()
    fun toMap(): Map<String, Any> = _intenalMap.map { it.key to it.value() }.toMap()
}


class MutableEnvBoundPropertyProvider<RECIEVER, RETTYPE : Any>(
    private val targetMap: MutableMap<String, () -> Any>,
    /**
     * Варианты имен
     */
    private val probes: List<String> = emptyList(),
    private val source: Map<String, String?> = System.getenv(),
    private val clazz: KClass<RETTYPE>,
    private val onInitializeHook: () -> Unit
) {

    private lateinit var value: RETTYPE
    private val locker = Any()

    operator fun provideDelegate(
        thisRef: RECIEVER,
        property: KProperty<*>
    ): ReadWriteProperty<RECIEVER, RETTYPE> {

        targetMap[property.name] = { getOrInit(property.name) }

        return object : ReadWriteProperty<RECIEVER, RETTYPE> {
            override fun getValue(thisRef: RECIEVER, property: KProperty<*>): RETTYPE {
                return getOrInit(property.name)
            }

            override fun setValue(thisRef: RECIEVER, property: KProperty<*>, value: RETTYPE) {
                this@MutableEnvBoundPropertyProvider.value = value
            }

        }
    }

    private fun getOrInit(name:String): RETTYPE {
        if (!::value.isInitialized) {
            synchronized(locker) {
                if (!::value.isInitialized) {
                    val names = probes.takeIf { it.isNotEmpty() } ?: listOf(name)
                    value = adaptValue(getDefault(names))
                    onInitializeHook()
                }
            }
        }
        return value
    }

    private fun getDefault(probes: List<String>): String {
        return probes.map {
            source[it] ?: source[it.toLowerCase()] ?: source[it.toUpperCase()]
        }.filterNotNull().firstOrNull() ?: ""
    }

    private fun adaptValue(value: String?): RETTYPE {
        return when {
            clazz == String::class -> (value ?: "") as RETTYPE
            clazz == Int::class -> (value.takeUnless { it.isNullOrBlank() } ?: "0").toInt() as RETTYPE
            else -> throw Exception("Not supported type")
        }
    }
}
