package codes.spectrum.blog.properties.е005

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.maps.shouldContainKey
import io.kotest.matchers.shouldBe
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty


/**
 * Фаза 5, сложное делегированное свойство через провайдер
 */
class EnvironmentBound(
    env: Map<String, String?> = System.getenv()
) : EnvironmentBoundBase(env) {
    var my_val: String by map()
    var some_other_val by map
    var my_int by int_map
    var renamed_val: Int by map("NEW_NAME", "OLD_NAME") {
        renamed_val_was_called = true
    }
    var renamed_val_was_called: Boolean = false
        private set

    val x by X
}

object X
inline operator fun X.provideDelegate(thisRef: Any?, property: KProperty<*>): ReadOnlyProperty<Any?,String> =
    object:ReadOnlyProperty<Any?,String>{
        override fun getValue(thisRef: Any?, property: KProperty<*>): String {
            return "hello"
        }
    }



internal class EnvironmentBoundTest : StringSpec() {
    init {

        "Любой объект как делегированное свойство"{
            val envbound = EnvironmentBound()
            envbound.x shouldBe "hello"
        }

        "Могу работать через карту" {
            val map = mapOf(
                "MY_VAL" to "X",
                "SOME_OTHER_VAL" to "Y",
                "OLD_NAME" to "4",
                "MY_INT" to "6",
                "X" to "X!"
            )
            val envbound = EnvironmentBound(map)

            /**
             * Все еще лениво по свойствам
             */
            envbound.renamed_val_was_called.shouldBeFalse()
            envbound.renamed_val shouldBe 4
            envbound.renamed_val_was_called.shouldBeTrue()

            /**
             * Но первое же обращение к toMap() гарантирует их инициализацию
             */
            envbound.toMap().also {
                it.shouldContainKey("renamed_val")
                it.shouldContainKey("some_other_val")
                it["some_other_val"] shouldBe "Y"
            }
            envbound.my_val shouldBe "X"
            envbound.some_other_val shouldBe "Y"
            envbound.my_int shouldBe 6

        }

    }
}