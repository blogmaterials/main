package codes.spectrum.blog.dsls.ddl.dsl

import comdivuz.kotlinblog.ddl.Column
import comdivuz.kotlinblog.ddl.Constraint
import comdivuz.kotlinblog.ddl.Table
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

class TableBuilder internal constructor() {
    var schema: String = ""
    var name: String = ""
    private val columns: MutableList<ColumnBuilder> = mutableListOf()
    private val constraints: MutableList<Constraint> = mutableListOf()

    object _NULL
    val NULL = _NULL

    inner class ColumnBuilder(
        var name: String = "",
        var type: String = "",
        var nullable: Boolean = true,
        var default: String? = null
    ) {
        fun build(): Column {
            return Column(
                name = name,
                type = type,
                nullable = nullable,
                default = default
            )
        }

        infix fun NOT(n: _NULL): ColumnBuilder {
            nullable = false
            return this
        }

        infix fun DEFAULT(sql: String): ColumnBuilder {
            default = sql
            return this
        }

        operator fun provideDelegate(thisRef: Nothing?, property: KProperty<*>): ReadOnlyProperty<Nothing?, Column> {
            name = property.name
            columns.add(this)
            return ReadOnlyProperty { _, _ -> this@ColumnBuilder.build() }
        }
    }


    val bigint get() = ColumnBuilder(type = "bigint")
    val text get() = ColumnBuilder(type = "text")
    val int get() = ColumnBuilder(type = "int")

    object _PRIMARY
    val PRIMARY = _PRIMARY
    infix fun _PRIMARY.KEY(column: Column) {
        constraints.add(Constraint.PrimaryKey(column))
    }

    inner class CheckBuilder (val sql:String) {
        operator fun provideDelegate(thisRef: Nothing?, property: KProperty<*>): ReadOnlyProperty<Nothing?,Constraint.Check> {
            val name = property.name
            val constraint = Constraint.Check(name, sql)
            constraints.add(constraint)
            return ReadOnlyProperty{_, _ -> constraint}
        }
    }
    fun CHECK(sql:String) = CheckBuilder(sql)

    fun build(): Table {
        return Table(
            schema = schema,
            name = name,
            columns = columns.map { it.build() },
            constraints = constraints.toList()
        )
    }
}

fun table(body: TableBuilder.() -> Unit): ReadOnlyProperty<Nothing?, Table> {
    return ReadOnlyProperty { _, property ->
        val builder = TableBuilder()
        val (schema, name) = property.name.split("__")
        builder.schema = schema
        builder.name = name
        builder.body()
        builder.build()
    }
}
