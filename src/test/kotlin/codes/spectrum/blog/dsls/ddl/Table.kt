package comdivuz.kotlinblog.ddl

import java.lang.StringBuilder

/**
 * Описатель таблицы
 */
data class Table(
    val schema: String,
    val name: String,
    val columns: List<Column> = listOf(),
    val constraints: List<Constraint> = listOf()
) : ISqlSource {
    val fullname = "$schema.$name"
    override fun toSql(): String {
        val sb = StringBuilder()
        sb.appendLine("CREATE TABLE IF NOT EXISTS $fullname (")
        val defs = (columns + constraints).map{"   ${it.toSql()}"}
        val inner_sql = defs.joinToString(",\n")
        sb.appendLine(inner_sql)
        sb.appendLine(");")
        return sb.toString()
    }
}
