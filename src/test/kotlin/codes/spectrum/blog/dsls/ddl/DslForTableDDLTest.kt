package comdivuz.kotlinblog.ddl

import codes.spectrum.blog.dsls.ddl.dsl.table
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import io.kotest.matchers.types.shouldBeInstanceOf
import io.kotest.matchers.types.shouldNotBeSameInstanceAs


/**
 * В целом хочется получить максимально наглядный язык для получения
 * из Kotlin генератора DDL Postgres для создания таблиц
 * ```
 * CREATE TABLE IF NOT EXISTS myschema.mytable (
 *    id bigint NOT NULL,
 *    name text NOT NULL DEFAULT 'noname',
 *    year int NULL,
 *    CONSTRAINT pk_id PRIMARY KEY (id),
 *    CONSTRAINT valid_year CHECK ( year is null OR (year > 1950 AND year <= 2030) )
 * )
 *
 * ```
 */
class DslForTableDDLTest : StringSpec() {

    /**
     * Показатели этого метода
     * 1. Количество символов 530 (хотя в SQL 247 с доп синтаксисом!) - даже если убрать именованные параметры кол-во символов > 400
     * 2. Количествло магических строк - 11, хотя явного SQL только 2
     * 3. Не похоже на SQL, сложно сравнивать
     */
    fun generateWithNativeAPI(): Table {
        val idColumn = Column("id", "bigint", nullable = false)
        return Table(
            schema = "myschema",
            name = "mytable",
            columns = listOf(
                idColumn,
                Column("name", "text", nullable = false, default = "'noname'"),
                Column("year", "int")
            ),
            constraints = listOf(
                Constraint.PrimaryKey(idColumn),
                Constraint.Check("valid_year", "year is null OR (year > 1950 AND year <= 2030)")
            )
        )
    }

    /**
     * Должны добиться
     * 1. Символов в Kotlin < чем SQL (!!! достигнуто 231 против 247 )
     * 2. Магические строки - только DEFAULT и CHECK (так как там произвольный SQL) (!!! достигнуто)
     * 3. Сделать максимально похоже на SQL чтобы было проще соотносить (!!! достигнуто)
     */
    fun generateWithDSL(): Table {
        val myschema__mytable by table {
            val id by bigint NOT NULL
            val name by text NOT NULL DEFAULT "'noname'"
            val year by int
            PRIMARY KEY id
            val valid_year by CHECK("year is null OR (year > 1950 AND year <= 2030)")
        }
        return myschema__mytable
    }

    init {

        "DSL формирует CHECK"{
            generateWithDSL().constraints[1].run {
                this.shouldBeInstanceOf<Constraint.Check>()
                this.definition shouldBe "year is null OR (year > 1950 AND year <= 2030)"
                this.name shouldBe "valid_year"
            }
        }

        "DSL формирует PRIMARY KEY"{
            generateWithDSL().constraints[0].run {
                this.shouldBeInstanceOf<Constraint.PrimaryKey>()
                this.column.name shouldBe "id"
            }
        }

        "DSL формирует колонки"{
            generateWithDSL().run {
                columns[0].run {
                    name shouldBe "id"
                    type shouldBe "bigint"
                    nullable shouldBe false
                    default.shouldBeNull()
                }
                columns[1].run {
                    name shouldBe "name"
                    type shouldBe "text"
                    nullable shouldBe false
                    default shouldBe "'noname'"
                }
                columns[2].run {
                    name shouldBe "year"
                    type shouldBe "int"
                    nullable shouldBe true
                    default.shouldBeNull()
                }
            }
        }

        "DSL генерация формирует схему и имя таблицы"{
            generateWithDSL().run {
                schema shouldBe "myschema"
                name shouldBe "mytable"
            }
        }

        "Проверяем, что DSL и нативное API возвращают один и тот же объект"{
            generateWithNativeAPI() shouldBe generateWithDSL()
        }


        "Проверяем, что Table действительно корректно сравнивается на equals"{
            generateWithNativeAPI() shouldBe generateWithNativeAPI()
            generateWithNativeAPI().copy(columns = listOf(Column("noid", "int"))) shouldNotBe generateWithNativeAPI()
            generateWithNativeAPI() shouldNotBeSameInstanceAs generateWithNativeAPI()

        }

        "Проверяем правильность самой генерации SQL" {
            // 530 символов в Kotlin и 430 символов SQL - мы не экономны
            generateWithNativeAPI().toSql().shouldBe(
                """CREATE TABLE IF NOT EXISTS myschema.mytable (
   id bigint NOT NULL,
   name text NOT NULL DEFAULT 'noname',
   year int NULL,
   CONSTRAINT pk_id PRIMARY KEY (id),
   CONSTRAINT valid_year CHECK (year is null OR (year > 1950 AND year <= 2030))
);
"""
            )
        }
    }
}
