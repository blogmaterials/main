package comdivuz.kotlinblog.ddl

/**
 * Описатель колонки
 */
data class Column (
    val name: String,
    val type: String,
    val nullable: Boolean = true,
    val default: String? = null
): ISqlSource {
    override fun toSql(): String {
        val sb = StringBuilder()
        sb.append(name)
        sb.append(" ")
        sb.append(type)
        sb.append(" ")
        if(!nullable){
            sb.append("NOT ")
        }
        sb.append("NULL")
        if(!default.isNullOrBlank()){
            sb.append(" DEFAULT ")
            sb.append(default)
        }
        return sb.toString()
    }

}
