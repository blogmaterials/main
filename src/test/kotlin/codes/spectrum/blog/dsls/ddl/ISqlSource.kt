package comdivuz.kotlinblog.ddl

interface ISqlSource {
    fun toSql(): String
}
