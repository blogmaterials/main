package comdivuz.kotlinblog.ddl

sealed class Constraint (open val name: String) : ISqlSource {
    override fun toSql(): String {
        return "CONSTRAINT $name ${internalSql()}"
    }
    protected abstract fun internalSql(): String

    data class PrimaryKey(val column: Column): Constraint("pk_${column.name}") {
        override fun internalSql(): String {
            return "PRIMARY KEY (${column.name})"
        }
    }

    data class Check(override val name: String, val definition: String): Constraint(name){
        override fun internalSql(): String {
            return "CHECK ($definition)"
        }
    }
}
