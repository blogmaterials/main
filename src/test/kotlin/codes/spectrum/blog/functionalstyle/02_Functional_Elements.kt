package codes.spectrum.blog.functionalstyle

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import kotlin.jvm.functions.FunctionN

class FirstClassFunction : StringSpec() {

    init {

        var myfun: (Int, Int) -> Int = { a, b -> a * b }

        val myfunTranspose: ((Int, Int) -> Int, Int) -> () -> Int = { f, x -> { f(x, x) + x } }

        class MySpecialFunc(val arg: Int) : (Int, Int) -> Int {
            override fun invoke(p1: Int, p2: Int): Int {
                return (p1 + arg) * (p2 - arg)
            }
        }


        "Оно работает!" {
            myfun = MySpecialFunc(3)
            myfunTranspose(myfun, 4)() shouldBe 11
        }


        class MySpecialFunc2(val arg: Int) : Function2<Int,Int,Int>{
            override fun invoke(p1: Int, p2: Int): Int {
                return (p1 + arg) * (p2 - arg)
            }
        }

        val nfunc : FunctionN<Int>? = null
        val longfunc : Function10<Int,Int,Int,Int,Int,Int,Int,Int,Int,Int,Int>? = null


        "Ламбда"{
            val s = SomeClass(5)
            val f = s.getFun()
            f(7) shouldBe 12
            `lambda$02_Functional_ElementsKt$39`(s)(7) shouldBe 12
            /**
            val clsfunref: KFunction1<SomeClass, (Int) -> Int> = SomeClass::getFun
            val objfunref: KFunction0<(Int) -> Int> =s::getFun
            **/
        }


    }
    class SomeClass(val x: Int) {
        fun getFun():(Int)->Int = {i -> this.x + i}
    }

    private class `lambda$02_Functional_ElementsKt$39`(private val self: SomeClass): Function1<Int,Int>{
        override fun invoke(p1: Int): Int {
            return self.x + p1
        }
    }
}
