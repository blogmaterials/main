package codes.spectrum.blog.functionalstyle

/**
 * Нам приходится идти на некоторое ухищрениче чтобы добиться
 * тейл рекурсивности
 */
private tailrec fun realRecoursiveIntPower(
    // база для возведения в степень
    basis: Double,
    // степень (остаточная)
    power: Int,
    // текущее значение результата
    current: Double,
    // признак отрицательной степени
    inverse: Boolean = false
): Double {
    return when {
        0 == power && inverse -> 1 / current
        0 == power && !inverse -> current
        // обеспечиваем хвостовую рекурсию с учетом отрицательных степеней
        else -> realRecoursiveIntPower(
            basis,
            // делаем положительный остаток от степени
            Math.abs(power) - 1,
            current * basis,
            // то есть это или при первом вызове, когда степень еще отрицательная
            // или в хвосте если уже был выставлен признак отрицательной степени
            inverse || power < 0
        )
    }
}

/**
 * Собственно аналог нашей ipow из Haskell и пример прототипирования функции относительно типа
 */
infix fun Double.ipower(power: Int): Double = realRecoursiveIntPower(this, power, 1.0)
infix fun Int.ipower(power: Int): Double = (this.toDouble() ipower power)


fun binaryPower( power: Int ) = 2 ipower power
