package codes.spectrum.blog.functionalstyle

import java.util.Date

val leted = 1.let { it * 2 }
val alsor = 1.also { println(it) }

fun tailfuncer(body: () -> Unit): Int {
    body()
    return 0
}

val tailfuncuser = {
    tailfuncer {
        println("me")
    }.let { it + 2 }
}

val collecioner = listOf(1, 2, 3, 4, 5).filter { it % 2 == 0 }.reduce { c, i -> c * i }

operator fun Date.plus(v: Int) = Date(this.time + v)

infix fun IntRange.expand(v: Int) :IntRange = (this.first - v) .. (this.last + v)
val willBe5to13 = 7..11 expand 2

fun destruction() {
    val (f, i , o) = "Иванов Иван Иванович".split(" ")
}

fun localfun(){
    var i = 1
    fun local() {i++}
    local()
    local()
}
