package codes.spectrum.blog.functionalstyle

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeInstanceOf
import java.util.concurrent.atomic.AtomicInteger

internal class FunctionalProgramming : StringSpec() {

    /**
     * От ламбд можно наследоваться! Это по сути интерфейс!
     */
    class MyIncrementFun(initial: Int = 0) : () -> Int {
        val state = AtomicInteger(initial)
        override fun invoke(): Int = state.incrementAndGet()
    }

    init {
        "Используем свой счетчик" {
            val inter: () -> Int = MyIncrementFun(5)
            inter() shouldBe 6
            inter() shouldBe 7
        }
    }

    /**
     * Нам приходится идти на некоторое ухищрениче чтобы добиться
     * тейл рекурсивности
     */
    private tailrec fun realRecoursiveIntPower(
        // база для возведения в степень
        basis: Double,
        // степень (остаточная)
        power: Int,
        // текущее значение результата
        current: Double,
        // признак отрицательной степени
        inverse: Boolean = false
    ): Double {
        fun evalResult() = if (inverse) 1.0 / current else current
        return when (power) {
            0 -> evalResult()
            // обеспечиваем хвостовую рекурсию с учетом отрицательных степеней
            else -> realRecoursiveIntPower(
                basis,
                // делаем положительный остаток от степени
                Math.abs(power) - 1,
                current * basis,
                // то есть это или при первом вызове, когда степень еще отрицательная
                // или в хвосте если уже был выставлен признак отрицательной степени
                inverse || power < 0
            )
        }
    }

    /**
     * Собственно аналог нашей ipow из Haskell и пример прототипирования функции относительно типа
     */
    infix fun Double.ipower(power: Int): Double = realRecoursiveIntPower(this, power, 1.0)
    infix fun Int.ipower(power: Int): Double = (this.toDouble() ipower power)

    init {
        "Степень" {
            (2 ipower 8) shouldBe 256
        }
        "Отрицательная степень" {
            (2 ipower -2) shouldBe 0.25
        }
    }

    interface IMonad<T> : () -> T {
        operator fun <N> plus(nextFunction: (T) -> N): Id<N>
    }

    /**
     * Реализуем обобщенную Id монаду для тождественного проброса значения
     */
    class Id<T>(private val provider: () -> T) : IMonad<T> {
        override fun invoke(): T = provider()
        override operator fun <N> plus(nextFunction: (T) -> N): Id<N> = Id({
            nextFunction(this())
        })
    }

    sealed class Calc {
        class Num(val i: Double) : Calc()

        interface Op {
            operator fun invoke(items: List<Calc>): List<Calc>
        }

        object Plus : Calc(), Op {
            override operator fun invoke(items: List<Calc>) =
                listOf(Num(items.map { (it as Num).i }.sum()))
        }

        object Mult : Calc(), Op {
            override operator fun invoke(items: List<Calc>) =
                listOf(Num(items.map { (it as Num).i }.reduce { a, c -> a * c }))
        }

        object Div : Calc(), Op {
            override operator fun invoke(items: List<Calc>) =
                listOf(Num(items.map { (it as Num).i }.reduce { a, c -> a * (1 / c) }))
        }

        companion object {
            operator fun invoke(s: String) = when {
                "+" == s -> Plus
                "*" == s -> Mult
                "/" == s -> Div
                else -> Num(s.toDouble())
            }

            fun calculate(calcs: Collection<Calc>): Double = calculate(emptyList(), calcs)

            // опять же достигаем хвостовой рекурсии
            tailrec fun calculate(stack: List<Calc>, agenda: Collection<Calc>): Double =
                when {
                    agenda.isEmpty() -> when (stack.size) {
                        0 -> error("ambigous")
                        1 -> when (val single = stack.first()) {
                            is Num -> single.i
                            else -> error("not number")
                        }
                        else -> error("ambigous")
                    }
                    stack.isEmpty() -> when (val next = agenda.first()) {
                        is Num -> calculate(stack + next, agenda.drop(1))
                        else -> error("apply operation on empty stack")
                    }
                    else -> when (val next = agenda.first()) {
                        is Op -> calculate(next(stack), agenda.drop(1))
                        else -> calculate(stack + next, agenda.drop(1))
                    }
                }
        }
    }

    init {

        "Монада с калькулятором и средой" {

            // Допустим это внешний мир (то есть функция зависящая от рандома
            var x = "1 2 +"

            fun calculator(src: () -> String) = Id(src) +
                { it.split(" ") } +
                { it.map { Calc(it) } } +
                { Calc.calculate(it) }

            val xcalculator = calculator { x }

            xcalculator.shouldBeInstanceOf<() -> Double>()
            xcalculator() shouldBe 3.0
            x = "1 2 3 + 4 * 2 3 /"
            xcalculator() shouldBe 4 // (((1+2+3) * 4) / 2) / 3

            calculator { "2 3 +" }() shouldBe 5
            calculator { "2 3 +" }() shouldBe 5
        }
    }
}
