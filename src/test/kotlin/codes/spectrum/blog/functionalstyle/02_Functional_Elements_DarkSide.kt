package codes.spectrum.blog.functionalstyle

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class FirstClassFunctionBad : StringSpec() {

    var global = 1

    init {

        class MySpecialNotFunc(var arg: Int) : (Int, Int) -> Int {
            override fun invoke(p1: Int, p2: Int): Int {
                global++
                return (p1 + arg++ + global) * (p2 - arg++ - global)
            }
        }

        "Это не функциональное программирование"{
            val f = MySpecialNotFunc(4)
            f(1,1) shouldBe -42
            f(1,1) shouldBe -90
            f(1,1) shouldBe -156
            f(0,0) shouldBe -240
        }

    }
}
