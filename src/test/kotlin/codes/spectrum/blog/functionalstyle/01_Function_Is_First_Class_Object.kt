package codes.spectrum.blog.functionalstyle

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeInstanceOf

// Function - это FirstClassObject

/**
 * Можно использовать функциональные типы
 * и инстанцировать при помощи lambda
 */
val square: (Int, Int) -> Int = { x, y -> Math.abs(x) * Math.abs(y) }

/**
 * Причем тут нет никакой магии, ничего сверх обычных
 * классов и методов, реализация ниже
 * `(Int,Int)->Int` это тоже самое что и `interface Function2<Int,Int,Int>`
 */
val square_noarrow: Function2<Int, Int, Int> = object : Function2<Int, Int, Int> {
    override fun invoke(x: Int, y: Int): Int {
        return Math.abs(x) * Math.abs(y)
    }

}

/**
 * Соответственно функция это не обязательно lambda или какая-то переменная
 * это может быть полноценный класс или объект, в котором что угодно
 * можно делать
 */
interface IWithMessage {
    val message: String
}

object Square : (Int, Int) -> Int, IWithMessage {
    /**
     * Какие-то дополнительные поля
     */
    override val message: String = "площадь"
    override fun invoke(x: Int, y: Int): Int {
        return Math.abs(x) * Math.abs(y)
    }
}

/**
 * Их легко композировать и получать функции из функций
 * f(x, y, f1) -> f1 ( f1(x, y), f1(y, x) )
 */
fun doubleInverseIntCall(func: (Int, Int) -> Int): (Int, Int) -> Int {
    return if (func is IWithMessage) {
        object : (Int, Int) -> Int, IWithMessage {
            override val message: String = "Вау! Кто то передал функцию как объект: ${func.message}"
            override fun invoke(x: Int, y: Int): Int {
                return func(func(x, y), func(y, x))
            }
        }
    } else { x: Int, y: Int -> func(func(x, y), func(y, x)) }
}

internal class doubleInverseIntCallTest : StringSpec() {
    init {
        "комбинирует и работает"{
            doubleInverseIntCall{x, y -> x - y}(4, 2) shouldBe ((4-2) - (2-4))
        }
        "типизированная"{
            val d = doubleInverseIntCall(Square)
            d.shouldBeInstanceOf<IWithMessage>()
            (d as IWithMessage).message shouldBe  "Вау! Кто то передал функцию как объект: площадь"
            d(3,4) shouldBe Square(Square(3,4), Square(4,3))
        }
    }
}
