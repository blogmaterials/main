package codes.spectrum.blog.functionalstyle

/**
 * Пример кода, вполне соответствующего ФП, но практически не читаемого
 */
class Templater(val pattern: String = "", val replacer: String = "") {
    private val regex: Regex? = pattern.takeIf { it.startsWith("/") && it.endsWith("/") }?.let {
        it.drop(1).dropLast(1)
    }?.toRegex()

    fun eval(s: String): String = (pattern.takeIf { !it.isNullOrBlank() }?.let {
        regex?.let { s.replace(it, replacer) } ?: s.replace(it, replacer)
    } ?: replacer).takeIf { it.isNotBlank() } ?: s
}

/**
 * Пример кода полностью противоречащего как ООП так и ФП,
 * но с кучей специальных конструкций.
 * Сделан на основе реального кода, объединяет 2 примера, не дословных
 */
class MyServiceRequest(val fio: String)
class MyServiceHttpClient(val url: String) {
    val parameters: MutableMap<String, String> = mutableMapOf()

    companion object {
        const val last_name_param = "last_name"
        const val first_name_param = "first_name"
        const val middle_name_param = "middle_name"
    }
    fun execute(request: MyServiceRequest) {

        (request toSplitted " ").let{
            it.preapare()
            val (last_name, first_name, middle_name) = it
            fun String.setParam(name: String){
                if(this.isNotBlank()){
                    if(!parameters.containsKey(name)){
                        parameters.put(name, this)
                    }else error("Задвоение параметра $name")
                }
            }
            last_name.let { it.setParam(last_name_param) }
            first_name.let { it.setParam(first_name_param) }
            middle_name.let { it.setParam(middle_name_param) }

            // далее какой-то вызов HTTP с передачей `parameters`
        }
    }

    private data class SplittedFio(var f: String, var i: String, var o: String){
        fun preapare(){
            f = if(f.isBlank()) error("Не указана фамилия") else f.replace("""\d+""".toRegex(),"")
            // попадались имена с цифрами
            i = if(i.isBlank()) error("Не указано имя") else i.replace("""\d+""".toRegex(),"")
            o = if(o.isBlank()) "" else o
        }
    }

    private infix fun MyServiceRequest.toSplitted(delimiter: String) =
        this.fio.trim().split(delimiter).filter { it.isNotBlank() }.map { it.trim() }.let {
            when (it.size) {
                1 -> it.let { val (f) = it; SplittedFio(f, "", "") }
                2 -> it.let { val (f, i) = it; SplittedFio(f, i, "") }
                3 -> it.let { val (f, i, o) = it; SplittedFio(f, i, o) }
                else -> it.take(3).let{val (f, i, o) = it; SplittedFio(f, i, o) }
            }
        }
}
