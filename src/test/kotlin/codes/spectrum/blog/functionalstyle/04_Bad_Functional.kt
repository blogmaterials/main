package codes.spectrum.blog.functionalstyle
/**
 * Все примеры "по мотивам"
 * реальные куски кода были очень разлаписты,
 * но при этом основной смысл ошибки сохранен
 */


/**
 * Некорректное использование let
 **/
class WithBadLet {
    var callCount : Int = 0
    fun doYourWorkBadLet() : Int {
        //1. по факту это не let, а also так как мы не получаем нового значения или определения
        return  callCount.let{
            callCount++ // 2. let это "положим", он не предназначен для сайд эффектов
            //3. да и вообще вчитайтесь
            it
        }
    }
    // однозначно лучше смотрелось бы так (то есть первый пример перемороченный и ненужный)
    fun doYourWorkBetter() : Int = callCount++
}

/**
 * Не понятная деструкция
 */
class StrangeDestruction {
    data class Rectangle(val weight:Double, val height:Double)
    enum class RectangleOrientation{
        SQUARE,
        HORIZONTAL,
        VERTICAL
    }
    /**
     * Опредеяем похожа ли фамилия на р
     */
    fun getRectangleOrientation(rect:Rectangle): RectangleOrientation {
        val (w, h) = rect // будет проблема если в классе Rectangle wight поменяют местами с height
        // Деструктурация это вообще симптом возможной жадности к чужим свойствам
        // И тут вообще очевидно что это должен был быть метод в самом Rectangle
        return when {
            w > h -> RectangleOrientation.HORIZONTAL
            w < h -> RectangleOrientation.VERTICAL
            else -> RectangleOrientation.SQUARE
        }
    }
}


/**
 * Использование ламбд и ссылок на функции без необходимости
 */
class SomeFinalClass (
    /**
     * семантика сбита - выглядит как подмена матчера, на деле расширения
     */
    private val customMatcher:  SomeFinalClass.(String) -> Boolean
){
    fun isMatch(s: String) : Boolean {
        val baseMatch = s.isNotBlank()
        return baseMatch && this.customMatcher(s)
    }
}

// а ведь достаточно было
open class SomeOpenClass () {
    open fun isMatch(s: String) = s.isNotBlank()
}

// или же если нужно обязательно сохранить s.isNotBlank()
open class SomeOpenClassFixed() {
    fun isMatch(s: String) = s.isNotBlank() && customIsMatch(s)
    open protected fun customIsMatch(s: String) = true
}


/**
 * Отказ от использования стандартных расширений
 * функция перемножения списка даблов
 */
fun selfMadeProduct(numbers: List<Double>): Double { // еще и ограничили тип на список
    if( numbers.isEmpty() ) error("Empty")
    var result = numbers[0]
    for( i in 1..numbers.size-1){
        result *= numbers[i]
    }
    return result
}

// это все вместо того чтобы написать
fun product(numbers: Iterable<Double>) = numbers.reduce{a, c -> a * c}
// это тот случай, когда функциональный стиль для стандартных коллекций и лаконичнее и читаемее и
