package codes.spectrum.blog.generics

import kotlin.reflect.KClass
import kotlin.reflect.full.createInstance

interface IAcceptor<T> {
    fun accept(other: T) : String
}
interface IFactory<T> {
    fun create(): T
    val name: String
}

class FactoryLogger : IAcceptor<LoggerFactory> {
    override fun accept(other: LoggerFactory) : String {
        return "accepted:${other.name}"
        val x = String::class
        x.createInstance()
    }
}
class LoggerFactory : IFactory<FactoryLogger>{
    override val name: String = "factory1"
    override fun create(): FactoryLogger {
        return FactoryLogger()
    }
}

class LoggerExtension<TLOGGER:IAcceptor<TFACTORY>,TFACTORY:IFactory<TLOGGER>>(private val factory:TFACTORY) {
    fun execute() : String = factory.create().accept(factory)
}


fun main() {
    val ext = LoggerExtension(LoggerFactory())
    assert(ext.execute() == "accept:factory1")
}
