package codes.spectrum.blog.generics

interface IImmutable<MUTABLE> {
    fun toMutable(): MUTABLE
}

interface IMutable<IMMUTABLE> {
    fun toImmutable(): IMMUTABLE
}

