package codes.spectrum.blog.extensions.chapter3_shugaring

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldContain
import io.kotest.matchers.string.shouldNotContain
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import java.time.Instant
import java.util.Date
import java.util.Locale
import kotlin.time.ExperimentalTime
import kotlin.time.measureTimedValue

class ShugaringTest : StringSpec() {

    /**
     * САХАР 1. Упрощаем использование неудобных внешних API
     *
     * Упрощение работы с внешними неудобными API и хелперами
     * Форматирование дабла в независимости от культуры (с точкой) и N цифрами
     * в дробной части.
     *
     * В Kotlin полно таких оберток над всякими Java-сложностями
     */
    fun Double.format(decimals: Int): String = String.format(Locale.ROOT, "%.0${decimals}f", this)

    init {
        "Проверка форматирования Double с заданным количеством десятичных знаков"{
            1.2265.format(2) shouldBe "1.23"
            0.2265.format(2) shouldBe "0.23"
        }
    }

    /**
     * САХАР 2. Унифицируем API
     * Мы ныпример знаем что у Date есть toInstant, а у Instant обратного
     * нет, и мы можем восполнить этот пробел в котлиновском духе
     */
    fun Instant.toDate() = Date(this.toEpochMilli())

    // кстати кто-то в курсе, что init в классе может быть несколько !
    init {
        "Проверяем Instant.toDate()"{
            Instant.ofEpochSecond(30).toDate() shouldBe Date(30000)
        }
    }

    /**
     * Сахар 3. Перегрузка операторов - их тоже можно исполнить как расширение
     */
    operator fun Date.plus(other: Date) = Date(this.time + other.time)
    operator fun Date.minus(other: Date) = Date(this.time - other.time)
    operator fun Date.plus(other: Long) = Date(this.time + other)
    operator fun Date.minus(other: Long) = Date(this.time - other)

    init {
        "Ну вот теперь можем еще и складывать даты как нам надо"{
            (Date(2000) + Date(3000)) shouldBe Date(5000)
            (Date(2000) + 3000) shouldBe Date(5000) // вау еще и смарткаст инта в Long
            (Date(2000) - Date(500)) shouldBe Date(1500)
            (Date(2000) - 500) shouldBe Date(1500)
        }
    }

    /**
     * Сахар 4. Чистый функциональный стиль - добавление расширений для
     * работы с функциями как первоклассными объектами и их преобразованиями
     */
    fun <S, M, R> ((M) -> R).compose(provider: (S) -> M): (S) -> R = { s -> this(provider(s)) }

    init {
        class SomeClassWithFunction {
            fun strToBool(str: String) = str.trim().toLowerCase().contains("true")
            fun boolToInt(boolean: Boolean) = if (boolean) 1 else 0
        }
        "Проверяем нашу супер-композирующую функцию"{
            /**
             * Работает на методах класса
             */
            val some = SomeClassWithFunction()
            val combine = some::boolToInt.compose(some::strToBool)
            combine("TRUE") shouldBe 1
            combine("treu") shouldBe 0

            /**
             * Работает на ламбдах
             */
            val strToBool = { str: String -> str.trim().toLowerCase().contains("true") }
            val boolToInt = { boolean: Boolean -> if (boolean) 1 else 0 }
            val combineLambda = boolToInt.compose(strToBool)
            combineLambda("TRUE") shouldBe 1
            combineLambda("treu") shouldBe 0
        }
    }

    /**
     * Сахар 5. Идеоматические конструкции и псевдооператоры
     * не будем затрагивать пока даже inline который еще больше
     * интересного порождает. Но в целом
     * в kotlin это уже конструкции типа let, also, run, apply, using,
     * runCatching
     *
     * в какой-то степени к ним же относятся и расширения коллекций.
     *
     * Свои даже сложно и придумать-то, но для тестов и всяких
     * декораторов можно и потрудиться написать:
     */
    @OptIn(ExperimentalTime::class)
    fun <R> CoroutineScope.printingAsync(out: StringBuilder, body: suspend () -> R): Deferred<R> = async {
        val stamp = Instant.now()
        val timedResult = measureTimedValue {
            out.appendLine("${Instant.now()} Start: ${stamp}")
            try {
                Result.success(body())
            } catch (e: Throwable) {
                Result.failure(e)
            }
        }
        if (timedResult.value.isFailure) {
            out.appendLine("${Instant.now()} Error: ${stamp} : ${timedResult.value.exceptionOrNull()!!.message}")
        } else {
            out.appendLine("${Instant.now()} Complete: ${stamp} : ${timedResult.value.getOrNull()}")
        }
        out.appendLine("${Instant.now()} Finish:${stamp}, duation: $timedResult.")
        timedResult.value.getOrThrow()
    }

    init {
        "Ну и проверяем как работает наше расширение"{
            var sb = StringBuilder()
            printingAsync(sb) {
                delay(1000)
                234
            }.await()
            sb.toString() shouldContain "Start"
            sb.toString() shouldContain "Complete"
            sb.toString() shouldContain "Finish"
            sb.toString() shouldNotContain "Error"
            sb.toString() shouldContain "234"

            sb = StringBuilder()
            try {
                coroutineScope {
                    printingAsync(sb) { //кстати если просто в контексте теста
                        // вызвать то ошибка НЕ обработается так как рухнет КОНТЕКСТ, такие дела
                        delay(1000)
                        throw Exception("Heh!")
                        234
                    }.await()
                }
            } catch (e: Throwable) {
            }
            sb.toString() shouldContain "Start"
            sb.toString() shouldNotContain "Complete"
            sb.toString() shouldContain "Finish"
            sb.toString() shouldContain "Error"
            sb.toString() shouldContain "Heh!"

        }
    }

    /**
     * Сахар 6. расширение компаньонов
     */
    //допустим в какой-то библиотеке мы определили интерфейс
    interface ISomeInterface {
        fun someMethod()

        companion object // просто как бы на вырост
    }

    // тогда например в библиотеке, которая реализует этот интерфейс можно сделать
    // например
    private class DbSomeImplementation : ISomeInterface {
        override fun someMethod() {
            // что-то тут происходит
        }
    }

    fun ISomeInterface.Companion.createDbImplementation(): ISomeInterface = DbSomeImplementation()

    val db = ISomeInterface.createDbImplementation()
    // внимание!
    // все сделано по DI - никуда не попадает конкрентная реализация

}
