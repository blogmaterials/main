package codes.spectrum.blog.extensions.chapter6_goodsandbads

import java.io.File
import java.io.Writer

// ГЛАВНОЕ! НЕ ДЕЛАЙТЕ РАСШИРЕНИЙ ЕСЛИ БЕЗ ЭТОГО МОЖНО ОБОЙТИСЬ!!!

/**
 * Плохо расширять Any - это крайне избыточно
 * засоряет все классы и очень общо.
 * Например встречается такое, не надо так делать.
 * Если где то понадобиься этакое проще локальное расширение
 * объявить.
 */
fun Any.anySize() = when (this) {
    is String -> this.length
    is Number -> this.toInt()
    is Collection<*> -> this.size
    else -> 0
}

/**
 * Плохо выносить в расширения базовых классов
 * вские не частые в использовании или узко доменные
 * методы - для этого надо делать хелперы или DSL
 */
fun String.makeMarkdownHeader1() = "# $this"

/**
 * Плохо делать расширения вместо методов, если
 * к этому нет никаких показаний
 */
class Rect(val width: Double, val height: Double)

val Rect.size get() = width * height

/**
 * Если используете как инварианты
 * Надо явно разделять имена в интерфейсах и имена
 * в расширениях чтобы исключить клэш и заодно обойти
 * проблему IDEA которая "не видит" расширений
 * если свой метод назван также
 */
interface ISomeWriter {
    fun nativeWrite(writer: Writer)
    // мы сознательно называем nativeWriter, чтобы явно выразить что это основной метод
    // к инвариантных использований
}

fun ISomeWriter.write(file: File) = file.writer().use { nativeWrite(it) }
fun ISomeWriter.write(writer: Writer) = nativeWrite(writer) // просто для унификации фасада - что все через write

/**
 * Все эти правила не распространяются на private, protected - там что угодно
 */
class SomeWithExtensions {
    /**
     * Вот тут в приватах что душе угодно
     */
    private val Any?.size get() = when (this) {
        is String -> this.length
        is Number -> this.toInt()
        is Collection<*> -> this.size
        else -> 0
    }
}
