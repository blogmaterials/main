package codes.spectrum.blog.extensions.chapter4_invariant

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import java.io.File
import java.io.StringWriter
import java.io.Writer

class InvariantsTest : StringSpec() {
    /**
     * Плохой и неудобный в использовании инетйфейс
     * все придется реализовывать и непонятно как связно использование
     * одних и других методов
     */
    interface IBadInterface {
        fun write(writer: Writer)
        fun write(): String
        fun write(file: File)
    }

    /**
     * Уже получше - имплементировать надо только
     * один метод, остальные - дефолты
     */
    interface INotBadNotGoodInterface {
        fun write(writer: Writer)
        fun write(file: File) = file.writer().use { write(it) }
        fun write() = StringWriter().also { write(it) }.toString()
    }

    // однако это не исключает перекрытия в духе
    class BadImplementation : INotBadNotGoodInterface {
        override fun write(writer: Writer) {
            writer.write("Hello")
        }

        // собственно нам ничего не мешает нарушить негласный контракт
        // интерфейса на то что все просто обертки над write(writer)
        override fun write(file: File) = TODO()

    }

    /**
     * И собственно единственный способ добиться желаемого
     * это расширения интерфейса
     */
    interface IBestInterface {
        fun write(writer: Writer)
    }

    fun IBestInterface.write(file: File) = file.writer().use { this.write(it) }
    fun IBestInterface.write() = StringWriter().also { write(it) }.toString()

    // теперь это инварианты интерфейса, которые нельзя будет
    // перекрыть и передать в составе интерфейса, все классы наследники будут
    // вести себя одинаково. Это можно сделать и final в абстрактном
    // классе, но в интерфейсе нельзя определить final

    /**
     * Это теперь interface c очень зажатым интерфейсом
     */
    interface IEvenBetterVariant {
        fun write(writer: Writer)

        /**
         * Прямо в него встроен инвариантный хелпер в обычной форме без расширений,
         * причем статический
         */
        companion object {
            @JvmStatic
            fun write(instance: IEvenBetterVariant, file: File) = file.writer().use { instance.write(it) }

            /**
             * Так как у этого варианта еще и изменяется возвращаемое значение
             * с Unit на String есть резон и имя чуть другое сделать
             */
            @JvmStatic
            fun writeToString(instance: IEvenBetterVariant) = StringWriter().also { instance.write(it) }.toString()
        }
    }

    /**
     * Если кому-то надо могут унаследовать и перекрыть
     * специальный расширенный интерфейс
     */
    interface IEvenBetterExtended : IEvenBetterVariant {
        /**
         * А он по сути трейт и вызывает опять же статический метод (нет дублирования)
         */
        fun write(file: File) = IEvenBetterVariant.write(this, file)
        fun writeToString() = IEvenBetterVariant.writeToString(this)
    }

    /**
     * А расширения "полируют" использование хоть перегруженного хоть статического варианта
     * через резолюцию типа
     */
    fun IEvenBetterVariant.write(file: File) =
        (this as? IEvenBetterExtended)?.write(file) ?: IEvenBetterVariant.write(this, file)

    fun IEvenBetterVariant.writeToString() =
        (this as? IEvenBetterExtended)?.writeToString() ?: IEvenBetterVariant.writeToString(this)

    /**
     * В итоге нам без разницы на что мы биндимся
     */
    class Simple: IEvenBetterVariant{
        override fun write(writer: Writer) {
            writer.write("hello")
        }
    }
    class Advanced: IEvenBetterExtended {
        override fun write(writer: Writer) {
            writer.write("world")
        }
        override fun writeToString(): String = "overriden: " + super.writeToString()
    }

    init {
        "Нам теперь без разницы какого конкретного типа объект и через что мы получили его функциональность"{
            val simple = Simple()
            simple.writeToString() shouldBe "hello" // используется расширение, но оно отправляет все на дефолт
            val advanced = Advanced()
            advanced.writeToString() shouldBe "overriden: hello" // используется метод класса
            (advanced as IEvenBetterVariant).writeToString() shouldBe "overriden: hello" // хотя теперь это расширение,
                // но проблемы больше нет с тем что не используется виртуальный метод
        }
    }
}
