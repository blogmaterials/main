package codes.spectrum.blog.extensions.chapter5_shadowing

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe

/***
 * Но такто расширения скорее зло чем добро и применять их надо очень и очень осторожно.
 * И главная их проблема - это статичность и низкий приоритет при линковке, различные
 * виды шейдинга
 */

// допустим есть иерархия
open class BaseClass{
    open fun x() = "base_x"
}
class ChildClass: BaseClass(){
    override fun x(): String = "child_x"
    fun y() = "child_y"
}
// и расширения
fun BaseClass.y() = "extended_base_y"
fun BaseClass.x() = "extended_base_x"
fun BaseClass.z() = "z_from_test"


class ShadowingTest: StringSpec() {
    init {
        "А теперь посмотрим как поведет себя биндинг"{
            val x = BaseClass()
            val y = ChildClass()
            x.x() shouldBe "base_x" // свой метод вытеснил расширение
            x.y() shouldBe "extended_base_y" // тут расширение
            y.x() shouldBe "child_x" //свой метод
            y.y() shouldBe "child_y" // снова свой
            // это более менее ожидаемо, но!!
            (y as BaseClass).x() shouldBe "child_x" // виртуальный метод не зависит от наших кастов типов,
            // он просто вызывается у переданного объекта, НО!
            (y as BaseClass).y() shouldBe "extended_base_y" // то есть
            y.y() shouldNotBe (y as BaseClass).y() //!!! а это конкретно выглядит как баг!!!
        }
    }
}

// еще тяжелее с бинарной совместимостью и скрытыми багами
// Допустим BaseClass живет в некоей библиотеки и в какой-то момент  в BaseClass
// добавят метод `y()` что будет происходить?
// 1. если ваш код был ранее собран и просто ему подсунули новый джарник то будет одна странность - код
// будет в упор игнорировать BaseClass.y() и продолжать вызывать расширение (байткод то не поменяется!!!)
// 2. если вы перекомпилите не зная, что в BaseClass уже есть y() вы будете продолжать считать, что
// выполняется ваше расширение, хотя будет выполняться уже метод BaseClass.x(), про который вы ничего не знаете!!
// НИКАКИХ ОСОБО СРЕДСТВ ОТ ЭТОГО ЗАЩИТИТЬСЯ НЕТ!!! И ЕСЛИ БЫ ЭТО БЫЛИ БЫ ХЕЛПЕРЫ ВСЕ БЫЛО БЫ ЧЕТКО,
// ТАК КАК ТАМ В КОДЕ ЯВНО ИСПОЛЬЗУЕМТСЯ КЛАСС ХЕЛПЕРА. НО В СЛУЧАЕ РАСШИРЕНИЙ КОМПИЛЯТОР И JVM НИКАК
// НЕ СТРАХУЮТ!!!
