package codes.spectrum.blog.extensions.chapter2_whatisnot


class WhatIsNotExtensionTest {
    class SomeClass(
        private val x: Int = 23,
        protected val y: Int = 1,
        public val z: Int = 1979
    )
    // они НЕ являются реально членами класса,
    // и соответственно имеют доступ только к публичной части
    //fun SomeClass.accessX() = x
    //не сработает fun SomeClass.accessY() = y
    fun SomeClass.accessZ() = z

    // соответственно ими нельзя доимплементировать интерфейс
    interface ISomeInterface {
        fun someMethod()
    }
    class SomeOtherClass /*не получится : ISomeInterface */  {

    }
    /* override */ fun SomeOtherClass.someMethod() {}

    /**
     * Ну и собственно мораль.
     * В первом приближении - расширения вообще не относятся к ООП,
     * на первый взгляд это просто синтаксический сахар для замены хелперов (!)
     * `Сахар` - это некие конструкции, которые ничего не добавляют
     * в функциональность как таковую, но улучшающие сам текст кода или простоту
     * его написания.
     */

}
