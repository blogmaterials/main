package codes.spectrum.blog.extensions.chapter1_whatis

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

/**
 * Мы можем расширить любой призвольный класс Java, даже системный
 */
private fun String.brace() = "($this)"

// внутри это такое:
//private fun brace(reciever: String) = "($reciever)"

class SomeClass {
    private fun String.a() = "a($this)"

    override fun toString(): String {
        fun String.b() = "b(${this.a()}"
        return "hello".b()
    }
}

/**
 * Мы можем расширять даже объекты с шаблонными параметрами
 */
private fun List<Int>.product(): Int = if (isEmpty()) 0 else reduce { c, n -> c * n }

/**
 * И собственно мы можем несколько шаблонов перекрыть по-разному
 */
private fun List<String>.product(): String = if (isEmpty()) "" else reduce { c, n -> "$c-$n" }

/**
 * И собственно мы можем несколько шаблонов перекрыть по-разному
 */
private fun List<Any>.product(): Any = if (isEmpty()) "" else reduce { c, n -> "$c~$n" }

/**
 * И это замена такому классу
 */
object MyHelper {
    fun brace(source:String) = "($source)"
    @JvmStatic
    fun product(intlist: List<Int>) = if (intlist.isEmpty()) 0 else intlist.reduce { c, n -> c * n }
    fun product(strlist: List<String>) = if (strlist.isEmpty()) "" else strlist.reduce { c, n -> "$c-$n" }
    fun product(anylist: List<Any>) = if (anylist.isEmpty()) "" else anylist.reduce { c, n -> "$c~$n" }
}

/**
 * Ну и просто убеждаемся, что все работает прекрасно!
 */
internal class WhatIsExtensionTest : StringSpec() {
    fun forByteCodeInvestigation() {
        "test".also {
            it.brace()
            MyHelper.brace(it)
        }
        listOf(2,3,4).also {
            it.product()
            MyHelper.product(it)
        }
        listOf("a", "b").also {
            it.product()
            MyHelper.product(it)
        }
    }

    init {


        "String.brace"{
            "test".brace() shouldBe "(test)"
        }
        "List<Int>.product()"{
            listOf(2, 3, 4).product() shouldBe 24
        }

        "List<String>.product()"{
            listOf("a", "b").product() shouldBe "a-b"
        }

        "List<Int> as Any.product()"{
            (listOf(2, 3, 4) as List<Any>).product() shouldBe "2~3~4"
        }


    }
}
