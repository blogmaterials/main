package codes.spectrum.blog.mocks.ex1_not_hard_to_implement

import codes.spectrum.blog.mocks.ISomeDbClient
import io.kotest.matchers.shouldBe

class Scenario(
    val finds: Int,
    val execs: Int,
    val execName: String,
    private val body: ISomeDbClient.() -> Unit
) : (ISomeDbClient) -> Unit {
    override fun invoke(p1: ISomeDbClient) {
        p1.body()
    }
}

/**
 * В этом сценарии проверяется, что
 * независимо от порядка find базы будет вызываться только 1 раз, exec 3 раза
 * и что имя для exec будет somename.test
 */
val UsualScenario by lazy {
    val resolvedName = "resolved.name"
    Scenario(1, 3, resolvedName, {
        this.execute()
        this.getrealname() shouldBe resolvedName
        this.execute(2)
        this.execute(3)
        this.getrealname() shouldBe resolvedName
    })
}
