package codes.spectrum.blog.mocks.ex1_not_hard_to_implement

import codes.spectrum.blog.mocks.BugUsualInterfaceClient
import codes.spectrum.blog.mocks.ISomeDbIface
import codes.spectrum.blog.mocks.PartiallyBugClient
import codes.spectrum.blog.mocks.WorkingUsualInterfaceClient
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import org.mockito.Mockito

internal class MockitoMockTest : StringSpec() {






    class MockitoBasedDbIface(val scenario: Scenario) : ISomeDbIface {
        private val db: ISomeDbIface
        init {
            db = Mockito.mock(ISomeDbIface::class.java)

            Mockito.`when`(db.find(Mockito.anyString())).thenAnswer {
                scenario.execName
            }

            Mockito.`when`(db.execute(Mockito.anyString(), Mockito.anyInt())).thenAnswer {
                (it.arguments[0] as String) shouldBe scenario.execName
                true
            }
        }
        fun verify() {
            Mockito.verify(db, Mockito.times(scenario.finds)).find(Mockito.anyString())
            Mockito.verify(db, Mockito.times(scenario.execs)).execute(Mockito.anyString(), Mockito.anyInt())
        }
        override fun find(name: String): String = db.find(name)
        override fun execute(name: String, data: Int): Boolean= db.execute(name, data)
    }


    init {
        "Пример теста с nиспользованием Mockito успешный"{
            UsualScenario.also { scenario ->
                MockitoBasedDbIface(scenario).also {
                    scenario(WorkingUsualInterfaceClient(it))
                }.verify()
            }
        }
        "Пример теста с nиспользованием Mockito  не успешный"{
            UsualScenario.also { scenario ->
                MockitoBasedDbIface(scenario).also {
                    scenario(BugUsualInterfaceClient(it))
                }.verify()
            }
        }
        "Пример теста с nиспользованием Mockito частично успешный"{
            UsualScenario.also { scenario ->
                MockitoBasedDbIface(scenario).also {
                    scenario(PartiallyBugClient(it))
                }.verify()
            }
        }
    }
}
