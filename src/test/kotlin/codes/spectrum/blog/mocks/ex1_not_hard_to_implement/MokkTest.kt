package codes.spectrum.blog.mocks.ex1_not_hard_to_implement

import codes.spectrum.blog.mocks.BugUsualInterfaceClient
import codes.spectrum.blog.mocks.ISomeDbIface
import codes.spectrum.blog.mocks.PartiallyBugClient
import codes.spectrum.blog.mocks.WorkingUsualInterfaceClient
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify

internal class MockkTest : StringSpec() {

    class MockkBasedDbIface(val scenario: Scenario) : ISomeDbIface {
        private val db: ISomeDbIface
        init {
            db = mockk()
            every {
                db.find(any())
            } returns scenario.execName

            every {
                db.execute(any<String>(), any<Int>())
            } .answers {
                (this.args[0] as String) shouldBe scenario.execName
                true
            }
        }
        fun verify() {
           verify(exactly = scenario.finds){ db.find(any())}
           verify(exactly = scenario.execs){ db.execute(any(), any())}
        }
        override fun find(name: String): String = db.find(name)
        override fun execute(name: String, data: Int): Boolean= db.execute(name, data)
    }


    init {
        "Пример теста с nиспользованием Mockk успешный"{
            UsualScenario.also { scenario ->
                MockkBasedDbIface(scenario).also {
                    scenario(WorkingUsualInterfaceClient(it))
                }.verify()
            }
        }
        "Пример теста с nиспользованием Mockk не успешный"{
            UsualScenario.also { scenario ->
                MockkBasedDbIface(scenario).also {
                    scenario(BugUsualInterfaceClient(it))
                }.verify()
            }
        }

        "Пример теста с nиспользованием Mockk частично успешный"{
            UsualScenario.also { scenario ->
                MockkBasedDbIface(scenario).also {
                    scenario(PartiallyBugClient(it))
                }.verify()
            }
        }
    }
}
