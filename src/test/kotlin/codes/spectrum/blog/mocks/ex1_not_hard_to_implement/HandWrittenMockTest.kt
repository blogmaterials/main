package codes.spectrum.blog.mocks.ex1_not_hard_to_implement

import codes.spectrum.blog.mocks.BugUsualInterfaceClient
import codes.spectrum.blog.mocks.ISomeDbIface
import codes.spectrum.blog.mocks.PartiallyBugClient
import codes.spectrum.blog.mocks.WorkingUsualInterfaceClient
import io.kotest.assertions.assertSoftly
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe


internal class HandWrittenMockTest : StringSpec() {

    class HandWrittenDbIFaceMock(val scenario: Scenario) : ISomeDbIface {
        override fun find(name: String): String {
            findCount++
            return scenario.execName
        }
        override fun execute(name: String, data: Int): Boolean {
            execCount++
            name shouldBe scenario.execName
            return true
        }
        var findCount: Int = 0
        var execCount: Int = 0
        fun verify() {
            assertSoftly {
                findCount shouldBe scenario.finds
                execCount shouldBe scenario.execs
            }
        }
    }


    init {
        "Успешный тест"{
            UsualScenario.also { scenario ->
                HandWrittenDbIFaceMock(scenario).also {
                    scenario(WorkingUsualInterfaceClient(it))
                }.verify()
            }

        }
        "Не успешный тест"{
            UsualScenario.also { scenario ->
                HandWrittenDbIFaceMock(scenario).also {
                    scenario(BugUsualInterfaceClient(it))
                }.verify()
            }
        }
        "Частично успешный тест"{
            UsualScenario.also { scenario ->
                HandWrittenDbIFaceMock(scenario).also {
                    scenario(PartiallyBugClient(it))
                }.verify()
            }
        }
    }
}
