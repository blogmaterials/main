package codes.spectrum.blog.iters

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.shouldBe

class SelfMadeSequenceTest2 : StringSpec() {
    init {
        "Секвенция работатет с методом yield как обычныек секвенции Kotlin"{
            SelfMadeSequnce2 {
                yield(1)
                yield(2)
                yield(3)
            }.toList() shouldBe listOf(1, 2, 3)
        }
        "Секвенция должна быть воспроизводимой"{
            val sequence = SelfMadeSequnce2 {
                yield(1)
                yield(2)
                yield(3)
            }
            sequence.toList() shouldBe listOf(1, 2, 3)
            sequence.toList() shouldBe listOf(1, 2, 3)
        }
        "Секвенция должна быть ленивой"{
            var wasCalled = false
            val sequence = SelfMadeSequnce2 {
                yield(1)
                yield(2)
                yield(3)
                wasCalled = true
            }
            sequence.take(2).toList() shouldBe listOf(1, 2)
            wasCalled.shouldBeFalse()
            sequence.toList() shouldBe listOf(1, 2, 3)
            wasCalled.shouldBeTrue()
        }
        "Секвенция должна спокойно пробрасывать исключения"{
            val sequence = SelfMadeSequnce2 {
                yield(1)
                yield(2)
                error("ошибка!")
                yield(3)
            }
            sequence.take(1).toList() shouldBe listOf(1)
            shouldThrow<Throwable> { sequence.toList() }.message shouldBe "ошибка!"

        }
    }
}