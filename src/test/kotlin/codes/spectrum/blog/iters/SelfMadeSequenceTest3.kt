package codes.spectrum.blog.iters

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

internal class SelfMadeSequenceTest3 : StringSpec() {
    init {
        "Собственно можем ей пользоваться"{
            // оказывается тейлы в конструкторах теперь поддерживаются!!!
            SelfMadeSequence2 {
                yield(1)
                yield(2)
                yield(3)
            }.toList() shouldBe listOf(1,2,3)

        }

        "Он точно ленивый"{
            var a = 0
            // оказывается тейлы в конструкторах теперь поддерживаются!!!
            SelfMadeSequence2 {
                yield(1)
                yield(2)
                yield(3)
                a = 3
            }.take(2).toList().also {
                it shouldBe listOf(1,2)
                a shouldBe 0
            }
            // оказывается тейлы в конструкторах теперь поддерживаются!!!
            SelfMadeSequence2 {
                yield(1)
                yield(2)
                a = 3
                yield(3)
            }.take(3).toList().also {
                it shouldBe listOf(1,2,3)
                a shouldBe 3
            }
        }

        "Воспроизводимое использование"{
            // оказывается тейлы в конструкторах теперь поддерживаются!!!
            val sequence = SelfMadeSequence2 {
                yield(1)
                yield(2)
                yield(3)
            }
            sequence.toList() shouldBe listOf(1,2,3)
            sequence.toList() shouldBe listOf(1,2,3)

        }

        "Обработка исключений"{
            val sequence = SelfMadeSequence2 {
                yield(1)
                yield(2)
                throw error("Тут у нас ошибка")
                yield(3)
            }
            sequence.take(2).toList() shouldBe listOf(1,2)
            shouldThrow<Throwable> { sequence.toList() }.also {
                it.message shouldBe "Тут у нас ошибка"
            }
        }
    }
}