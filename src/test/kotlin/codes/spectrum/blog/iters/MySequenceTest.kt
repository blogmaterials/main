package codes.spectrum.blog.iters

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.shouldBe
import kotlin.concurrent.thread
import kotlin.experimental.ExperimentalTypeInference

class MySequenceTest : StringSpec() {
    init {
        "Берет на вход блок с yield и может из него сделать перечисление"{
            val values: List<Int> = MySequence {
                yield(1)
                yield(2)
                yield(3)
            }.toList()
            values shouldBe listOf(1, 2, 3)
        }

        "Воспроизодимость"{
            val s = MySequence {
                yield(1)
                yield(2)
                yield(3)
            }
            s.toList() shouldBe listOf(1, 2, 3)
            s.toList() shouldBe listOf(1, 2, 3)
        }

        "Ленивость"{
            var wasCalled = false
            val s = MySequence {
                yield(1)
                yield(2)
                yield(3)
                wasCalled = true
            }
            s.toList().also {
                it shouldBe listOf(1, 2, 3)
                wasCalled.shouldBeTrue()
            }
            wasCalled = false
            s.take(1).toList().also {
                it shouldBe listOf(1)
                wasCalled.shouldBeFalse()
            }
        }

        "Корректная обработка ошибок"{
            val s = MySequence {
                yield(1)
                yield(2)
                error("ошибка!")
                yield(3)
            }
            s.take(1).toList() shouldBe listOf(1)
            shouldThrow<IllegalStateException> { s.toList() }.message shouldBe "ошибка!"
        }
    }
}

interface IYielder<T> {
    fun yield(value: T)
}

@OptIn(ExperimentalTypeInference::class)
class MySequence<T>(@BuilderInference val block: IYielder<T>.() -> Unit) : Iterable<T> {
    override fun iterator(): Iterator<T> {
        return InnerIterator()
    }


    inner class InnerIterator : Iterator<T> {

        var lock = Object()
        var currentValue: T? = null
        var hasValue: Boolean = false
        var error: Throwable? = null

        inner class Yielder : IYielder<T> {
            override fun yield(value: T) {
                currentValue = value
                hasValue = true
                lock.notify()
                lock.wait()
            }
        }

        init {
            thread {
                synchronized(lock) {
                    try {
                        val yielder = Yielder()
                        yielder.block()
                    } catch (e: Throwable) {
                        error = e
                    } finally {
                        lock.notifyAll()
                    }
                }
            }
        }

        private fun checkError(){
            if(error != null) {
                throw error!!
            }
        }

        override fun hasNext(): Boolean {
            checkError()
            synchronized(lock) {
                if (hasValue) {
                    return true
                }
                lock.wait()
                checkError()
                return hasValue
            }
        }

        @Suppress("UNCHECKED_CAST")
        override fun next(): T {
            checkError()
            synchronized(lock) {
                if (hasNext()) {
                    val value = currentValue
                    hasValue = false
                    lock.notify()
                    return value as T
                } else {
                    error("read from closed iterator")
                }
            }
        }

    }
}
