package codes.spectrum.blog.iters

import kotlin.concurrent.thread
import kotlin.experimental.ExperimentalTypeInference

interface ILazyScope<T> {
    fun yield(value: T)
}

@OptIn(ExperimentalTypeInference::class)
class SelfMadeSequnce2<T>(@BuilderInference val block: ILazyScope<T>.() -> Unit) : Iterable<T> {
    override fun iterator(): Iterator<T> {
        return InnerIterator()
    }

    private inner class InnerIterator : Iterator<T> {
        var hasValue: Boolean = false
        var currentValue: T? = null
        val lock = Object()
        var error: Throwable? = null

        private inner class LazyScope : ILazyScope<T> {
            override fun yield(value: T) {
                currentValue = value
                hasValue = true
                lock.notify()
                lock.wait()
            }
        }

        init {
            thread {
                synchronized(lock) {
                    try {
                        val scope = LazyScope()
                        scope.block()
                    } catch (e: Throwable) {
                        error = e
                    } finally {
                        lock.notifyAll()
                    }
                }
            }
        }

        fun checkError() {
            if (null != error) {
                throw error!!
            }
        }

        override fun hasNext(): Boolean {
            checkError()
            synchronized(lock) {
                if (hasValue) {
                    return true
                }
                lock.wait()
                checkError()
                return hasValue
            }
        }

        override fun next(): T {
            checkError()
            synchronized(lock) {
                if (hasNext()) {
                    hasValue = false
                    val value = currentValue
                    lock.notify()
                    return value as T
                } else {
                    error("read from closed iterator")
                }
            }
        }
    }
}
