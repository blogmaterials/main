package codes.spectrum.blog.stdlib

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import kotlin.experimental.ExperimentalTypeInference

@OptIn(ExperimentalTypeInference::class)
object NumberCreator {

    @OverloadResolutionByLambdaReturnType
    fun create(intProvider: () -> Int): Int {
        return intProvider() * 2
    }

    fun create(doubleProvider: () -> Double): Double {
        return doubleProvider() * 2.2
    }

}

class MyTest : StringSpec() {
    init {
        "работа с Int"{
            val i: Int = NumberCreator.create{ 123 }.also { it shouldBe 123 * 2 }
        }
        "работа с Double"{
            val i: Double = NumberCreator.create{ 123.45 }.also { it shouldBe 123.45 * 2.2 }
        }
    }
}