package codes.spectrum.blog.stdlib

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

fun someEvenFunction(i: Int) = i * 2
fun someOddFunction(i: Int) = i * 3
fun someRecursiveFunction(deep: Int): Int {
    return when {
        deep <= 0 -> 0
        deep % 2 == 0 -> someEvenFunction(deep) + someRecursiveFunction(deep - 1)
        else -> someOddFunction(deep) + someRecursiveFunction(deep - 1)
    }
}

@OptIn(ExperimentalStdlibApi::class)
fun deepSomeRecursiveFunction(i: Int): Int = DeepRecursiveFunction<Int, Int> { deep ->
    when {
        deep <= 0 -> 0
        deep % 2 == 0 -> someEvenFunction(deep) + callRecursive(deep - 1)
        else -> someOddFunction(deep) + callRecursive(deep - 1)
    }
}(i)

class DeepRecursionFunctionTest : StringSpec() {
    init {
        "Проверим что некая функция работает на небольшой глубине рекурсии"{
            someRecursiveFunction(10) shouldBe (10 * 2 + 9 * 3 + 8 * 2 + 7 * 3 + 6 * 2 + 5 * 3 + 4 * 2 + 3 * 3 + 2 * 2 + 1 * 3 + 0)
        }

        "Проверим что на большой глубине у функции возникает проблема рекурсии"{
            shouldThrow<StackOverflowError> { someRecursiveFunction(100000) }
        }

        /**
         * Что делать?
         */

        "Нашли способ, проверили, что работает на малой глубине"{
            deepSomeRecursiveFunction(10) shouldBe (10 * 2 + 9 * 3 + 8 * 2 + 7 * 3 + 6 * 2 + 5 * 3 + 4 * 2 + 3 * 3 + 2 * 2 + 1 * 3 + 0)
        }

        "Нашли способ, проверили, что работает на большой глубине"{
            deepSomeRecursiveFunction(1000000 ) shouldBe 165516864
        }
    }
}