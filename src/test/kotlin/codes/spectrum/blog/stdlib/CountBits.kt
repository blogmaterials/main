package codes.spectrum.blog.stdlib

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe


fun Long.bitCount() = recursoveBitCount(result = 0, current = this.toULong())
private tailrec fun recursoveBitCount(result:Int, current:ULong):Int {
    return when {
        current == 0UL -> result
        (current and 1UL) == 0UL -> recursoveBitCount(result, current shr 1)
        else -> recursoveBitCount(result + 1, current shr 1)
    }
}

class CountBitsTest : StringSpec() {
    init {
        "положительные Long"{
            2L.bitCount() shouldBe 1
            5L.bitCount() shouldBe 2
            7L.bitCount() shouldBe 3
        }
        "отрицательные Long"{
            (-1L).bitCount() shouldBe 64
            (-2L).bitCount() shouldBe 63
        }

        "положительные Long countOneBits"{
            2L.countOneBits() shouldBe 1
            5L.countOneBits() shouldBe 2
            7L.countOneBits() shouldBe 3
        }
        "отрицательные Long countOneBits"{
            (-1L).countOneBits() shouldBe 64
            (-2L).countOneBits() shouldBe 63
        }
    }
}