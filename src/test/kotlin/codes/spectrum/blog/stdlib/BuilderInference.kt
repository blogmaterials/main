package codes.spectrum.blog.stdlib

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import kotlin.experimental.ExperimentalTypeInference

class MySuperCollectionScope<T:Any>(val maxWordSize: Int){
    val result: MutableList<T> = mutableListOf()
    fun yield(value: T) {
        if(value.toString().length <= maxWordSize){
            result.add(value)
        }
    }
}

@OptIn(ExperimentalTypeInference::class)
fun <T:Any> mySuperCollection(maxWordSize: Int, @BuilderInference block : MySuperCollectionScope<T>.()->Unit): List<T> {
    val scope = MySuperCollectionScope<T>(maxWordSize)
    scope.block()
    return scope.result.toList()
}

class FunctionParameterOverloadTest : StringSpec() {
    init {
        "коллекция маленьких значений с билдером - строки"{
            val myStringList : List<String> = mySuperCollection(3) {
                yield("hello")
                yield("top")
                yield("12312312")
                yield("on")
            }.also{ it shouldBe listOf("top","on")}

            val mixedList : List<Any> = mySuperCollection(3) {
                yield("hello")
                yield("top")
                yield("12312312")
                yield("on")
                yield(123)
            }.also{ it shouldBe listOf("top","on", 123)}
        }
        "коллекция маленьких значений с билдером - числа"{
            val myIntList : List<Int> = mySuperCollection(3) {
                yield(12345)
                yield(123)
                yield(135421343)
                yield(12)
            }.also{ it shouldBe listOf(123,12)}
        }
    }
}