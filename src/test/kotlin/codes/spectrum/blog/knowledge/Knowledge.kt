@file:Suppress("ALL")

package codes.spectrum.knowledge

import codes.spectrum.knowledge.IWithIdentity.Companion.IDENTITY_JOINER
import io.kotest.assertions.assertionCounter
import io.kotest.assertions.eq.eq
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe

/**
 * Маркер того, что объект является носителем идентичности
 * Контракт:
 * 1. toString() полностью выражает идентичность
 * 2. toString() гарантирует стабильность
 * 3. equals() сравнивает только toString() с ЛЮБЫМ объектом!!!
 *
 * при этом структура самозамкнутая, идентичная сама себе
 */
interface IIdentity : IWithIdentity

/**
 * Интерфейс носителя идентичности
 */
interface IWithIdentity {
    val identity: IIdentity

    companion object {
        /**
         * Строка для объединения идентичностей
         */
        const val IDENTITY_JOINER = ":::"
    }
}

/**
 * Специальный объект для обозначения отсутствия идентичности
 */
internal object UndefinedIdentity : Identity("UNDEFINED_IDENTITY")
internal interface IIdentityPlaceHolder
internal object SinglePlaceHolderIdentity : Identity("*"), IIdentityPlaceHolder
internal object MultiplePlaceHolderIdentity : Identity("**"), IIdentityPlaceHolder

val IIdentity.isUndefined get() = this === UndefinedIdentity
val IIdentity.isDefined get() = !isUndefined
val IIdentity.isPlaceholder get() = this is IIdentityPlaceHolder
val IIdentity.isMultiplePlaceHolder get() = this == MultiplePlaceHolderIdentity
val IIdentity.isSinglePlaceHolder get() = this == SinglePlaceHolderIdentity

fun identityOf(obj: Any?): IIdentity = Identity(obj)

fun IIdentity.matches(other: IIdentity) = when {
    this is UndefinedIdentity || other is UndefinedIdentity -> false
    this is IIdentityPlaceHolder || other is IIdentityPlaceHolder -> true
    else -> this == other
}

private fun List<IIdentity>.reduceWildCards(): List<IIdentity> {
    if (!this.contains(MultiplePlaceHolderIdentity)) return this
    val result = mutableListOf<IIdentity>()
    for ((i, v) in this.withIndex()) {
        if (v != MultiplePlaceHolderIdentity) {
            result.add(v)
        } else {
            if (!(i > 0 && result[i - 1] != MultiplePlaceHolderIdentity)) {
                result.add(v)
            }
        }
    }
    return result.toList()
}

fun IIdentity.split(): List<IIdentity> = this.toString().split(IDENTITY_JOINER).map {
    identityOf(it)
}.let {
    it.filterIndexed { index, iIdentity ->
        iIdentity != MultiplePlaceHolderIdentity || index <= 0 || it[index - 1] != MultiplePlaceHolderIdentity
    }
}

fun IWithIdentity.split(): List<IIdentity> = this.identity.split()

fun IWithIdentity.matches(other: IWithIdentity) = this.identity.matches(other.identity)

/**
 *
 */
fun IIdentity.matchesPattern(other: IIdentity): Boolean {
    val self_parts: List<IIdentity> = this.split()
    val other_parts: List<IIdentity> = other.split()
    tailrec fun matchPatternIndexed(

        si: Int = 0,
        oi: Int = 0
    ): Boolean {
        // если уже все обощли и не нашли расхождений, значит все, выход с true
        if (oi >= other_parts.size && si >= self_parts.size) return true
        // если что-то обошли а что-то нет сразу выход с false
        if (oi >= other_parts.size || si >= self_parts.size) return false
        val s = self_parts[si]
        val o = other_parts[oi]
        // если текущие позиции не совпадают то сразу выход
        if (!s.matches(o)) return false
        if (oi == other_parts.size - 1 && o == MultiplePlaceHolderIdentity) return true
        if (si == self_parts.size - 1 && s == MultiplePlaceHolderIdentity) return true
        if (s == MultiplePlaceHolderIdentity && matchPatternIndexed(si + 1, oi)) return true
        if (o == MultiplePlaceHolderIdentity && matchPatternIndexed(si, oi + 1)) return true
        // обошли все и в конце остался множественный плейсхолдер **

        // промотка  с учетом множественных плейсхолдеров
        return if (s == MultiplePlaceHolderIdentity && o != MultiplePlaceHolderIdentity) {
            matchPatternIndexed(si, oi + 1)
        } else if (s != MultiplePlaceHolderIdentity && o == MultiplePlaceHolderIdentity) {
            matchPatternIndexed(si + 1, oi)
        } else {
            matchPatternIndexed(si + 1, oi + 1)
        }
    }
    return matchPatternIndexed()
}

fun IWithIdentity.matchesPattern(other: IWithIdentity) = this.identity.matchesPattern(other.identity)

/**
 * Абстрактная реализация контракта [IIdentity]
 */
open class Identity protected constructor(
    private val identityString: String
) : IIdentity {
    final override fun toString() = identityString
    final override fun equals(other: Any?) = other?.toString() == this.toString()
    final override fun hashCode(): Int = identityString.hashCode()

    // самозакмкнутая структура
    override final val identity: IIdentity = this

    companion object {
        operator fun invoke(obj: Any?): IIdentity {
            return when {
                obj == null -> UndefinedIdentity
                obj is IIdentity -> obj
                obj is IWithIdentity -> obj.identity
                obj is String && obj == "*" -> SinglePlaceHolderIdentity
                obj is String && obj == "**" -> MultiplePlaceHolderIdentity
                else -> Identity(idobjectToString(obj))
            }
        }

        private fun idobjectToString(idobject: Any): String = when {
            // особое поведение для коллекций, приводим их к строке особым образом
            idobject is Collection<*> -> idobject.joinToString()
            // остальное пока просто через обычный toString()
            else -> idobject.toString()
        }
    }
}

/**
 * Оператор комбинации идентичностей
 */
operator fun IIdentity.plus(other: IIdentity): IIdentity {
    return when {
        this.isUndefined -> other
        other.isUndefined -> this
        else -> Identity(this.toString() + IDENTITY_JOINER + other.toString())
    }
}

/**
 * Абстракция типизации типа знаний, это не Enum, а открытая иерархия
 * @property identity строка идентичности факта
 * @param T делаем типы фактов генериками прежде всего для смарткастов  и более четкой типизации
 */
abstract class FactType(override val identity: IIdentity) : IWithIdentity {
    /**
     * Добавляем удобный конструктор от строки
     */
    constructor(identity: String) : this(identityOf(identity))

    // по умолчанию идентичность и есть строковое представление
    override fun toString() = identity.toString()
}

/**
 *  Для знаний характерно понятие релевантности, достоверности, обобщить можно
 * как "подтвержденность" и это некое состояние факта
 */
enum class FactState {
    /**
     * Подтвержденный факт, не может быть больше отменен
     */
    Confirmed,

    /**
     * Факт в чьей корректности уверен публикатор факта
     * если не будет отменен, то считается [Confirmed]
     */
    Asserted,

    /**
     * Неоднозначный факт, если не будет явно [Confirmed] или [Asserted] считается [Declined]
     */
    Ambigous,

    /**
     * Отклоненный факт (это то, что до этого называлось isRemoved)
     */
    Declined
}

/**
 * Абстракция единицы знания, обладает типом, происхождением, идентичностью и актуальностью
 * контракт:
 * 1. идентичность факта - это идентичность самих данных в нем, источника и типа
 * 1. equals и hashCode зависят ТОЛЬКО от идентичности
 * 2. toString() ОБЯЗАН включать в себя тип, идентичность и статус
 * 3. toString() МОЖЕТ дополнительно включть любую расширенную информацию
 */
interface IFact : IWithIdentity {
    /**
     * Тип факта
     */
    val type: FactType

    /**
     * Источником может быть что угодно с идентичностью,
     * включая и сам объект [Identity]
     */
    val source: IIdentity

    /**
     * Состояние
     */
    var state: FactState
}

/**
 * Абстрактная реализция контракта факта
 * @property type тип факта
 * @property definer определитель факта, любого типа, носитель специфических знаний
 */
abstract class FactBase<DEFINER : Any>(
    override final val type: FactType,
    val definer: DEFINER,
    override final val source: IIdentity
) : IFact {

    /**
     * Статус факта, изменяемый
     */
    override final var state: FactState = FactState.Asserted

    private val definerIdentity by lazy { identityOf(definer) }

    /**
     * Выполнена, причем жестко та часть контракта, которая гласит, что идентичность факта -
     * это только комбинация идентичности типа и опеределителя знаний
     * lazy чтобы обойти проблему нефинальных типов в конструкторе
     */
    final override val identity by lazy { source.identity + type.identity + definerIdentity }

    /**
     * Эквивалентность реагирует ТОЛЬКО на идентичность!
     */
    final override fun equals(other: Any?): Boolean {
        return identity == identityOf(other)
    }

    /**
     * Hash зависит только от идентичности
     */
    final override fun hashCode() = identity.hashCode()

    /**
     * Точка расширения для дочерних фактов для добавления дополнительной информации в toString()
     */
    protected open val extendedStringInfo = ""

    /**
     * Жестко зашиваем формат представления факта в виде строки
     * четко по контркту - ничего лишнего - есть статус, происхождение идентичность и точка расширения для допю данных
     * плюс загоняем все в одну строку гарантировано
     */
    final override fun toString(): String {
        val builder = StringBuilder()
        builder.append("(")
        builder.append(state)
        builder.append(")")
        if (source.isDefined) {
            builder.append(" ")
            builder.append(source.identity)
            builder.append(" ->")
        }
        builder.append(" ")
        builder.append(type)
        builder.append(" : [")
        builder.append(definerIdentity)
        builder.append("]")
        if (extendedStringInfo.isNotBlank()) {
            builder.append(" : ")
            builder.append(extendedStringInfo)
        }
        val result = builder.toString().replace(SPACES_REGEX, " ").trim()
        return result
    }

    companion object {
        private val SPACES_REGEX = """\s+""".toRegex()
    }
}

/**
 * Просто класс для ручной настройки факта
 */
class GenericFact<DEFINER : Any>(
    type: FactType,
    definer: DEFINER,
    source: IIdentity = UndefinedIdentity
) : FactBase<DEFINER>(
    type = type,
    definer = definer,
    source = source
)

// И вот как это применяется уже в конкретных знаниях

/**
 * Определим простой факт от одного слова
 */
class TermFact(
    val term: String,
    source: IIdentity = UndefinedIdentity
) : FactBase<String>(
    type = SelfType,
    definer = term,
    source = source
) {
    private object SelfType : FactType("term")
}

/**
 * Определим специфические данные для перевода
 */
data class Translation(val sourceLang: String, val resultLang: String, val sourceTerm: String, val resultTerm: String) {
    override fun toString() = "$sourceTerm ($sourceLang) -> $resultTerm ($resultLang)"
}

class TranslationFact(
    val translation: Translation,
    source: IIdentity = UndefinedIdentity
) : FactBase<Translation>(
    type = SelfType,
    definer = translation,
    source = source
) {
    private object SelfType : FactType("translation")
}

internal class KnowledgeTest : StringSpec() {
    init {
        "Идентичность формируется из чего угодно и тождественна по сравнению строк"{
            Identity(123) shouldBe 123
            Identity(123) shouldBe "123"
            Identity(123) shouldBe Identity("123")

            Identity(listOf("1", 2)) shouldBe Identity(listOf(1, "2"))
            Identity(listOf("1", 2)) shouldBe "1, 2"
        }

        "Идентичности можно складывть с игнорированием Undefined и разбирать обратно"{
            val identity = Identity(123) +
                UndefinedIdentity +
                SinglePlaceHolderIdentity +
                Identity("hello")

            identity shouldBe "123:::*:::hello"
            // не нужно парсинга - это эквивалентное создание
            identity shouldBe Identity("123:::*:::hello")
            // и опять же мы можем их рассматривать одновременно как идентити и их простое отображение
            identity.split() shouldBe listOf(123, "*", "hello")
            identity.split() shouldBe listOf(Identity(123), SinglePlaceHolderIdentity, Identity("hello"))

        }


        infix fun IWithIdentity.shouldMatchPattern(pattern: Any) {
            val actual = this.matchesPattern(identityOf(pattern))
            assertionCounter.inc()
            eq(actual, true)?.let { throw it }
        }

        infix fun IWithIdentity.shouldNotMatchPattern(pattern: Any) {
            val actual = this.matchesPattern(identityOf(pattern))
            assertionCounter.inc()
            eq(actual, false)?.let { throw it }
        }

        "Идентичности можно сравнивать с паттерном (смотрим что с завершением)"{
            val identity = identityOf("a:::b:::c:::d")
            identity shouldMatchPattern "**"
        }

        "Идентичности можно сравнивать с паттерном (баг с хидером)"{
            val identity = identityOf("a:::b:::c:::d")
            identity shouldMatchPattern "**:::c:::d"
        }

        "Идентичности с серединным ** (баг с хидером)"{
            val identity = identityOf("a:::b:::c:::d")
            identity shouldNotMatchPattern "a:::**:::e"
        }

        "Идентичности с двумя серединными ** (баг с хидером)"{
            val identity = identityOf("a:::b:::c:::d")
            identity shouldNotMatchPattern "a:::**:::**:::e"
        }


        "Идентичности можно сравнивать с паттерном"{
            val identity = identityOf("a:::b:::c:::d")
            identity shouldMatchPattern "*:::*:::c:::d"
            identity shouldMatchPattern Identity("*:::b:::*:::d")
            identity shouldMatchPattern "a:::b:::*:::d"
            identity shouldMatchPattern "a:::*:::*:::d"
            identity shouldMatchPattern "**"
            identity shouldMatchPattern "**:::d"
            identity shouldMatchPattern "a:::**"
            identity shouldMatchPattern "a:::**:::d"
            identity shouldMatchPattern "a:::**:::*:::d"
            identity shouldMatchPattern "a:::*:::**:::d"
            identity shouldMatchPattern "a:::**:::c:::**:::d"
            identity shouldMatchPattern "a:::**:::**:::d"
            identity shouldNotMatchPattern "a:::c:::*:::d"
            identity shouldNotMatchPattern "*"
            identity shouldNotMatchPattern "a:::**:::e"
            identity shouldNotMatchPattern "a:::**:::c:::**:::e"
            identity shouldNotMatchPattern "a:::**:::**:::e"
        }

        "Факт также строятся как идентичность и важно именно равенство идентичностей"{
            TermFact("hello") shouldBe "term:::hello"
            TermFact("hello").term shouldBe "hello"
            // статус не влияет на идентичность
            TermFact("hello").apply { state = FactState.Confirmed } shouldBe "term:::hello"

            // а происхождение влияет
            TermFact("hello", source = Identity("me")) shouldNotBe "term:::hello"
            TermFact("hello", source = Identity("me")) shouldBe "me:::term:::hello"
        }

        "Факт может быть очень типизированным и при этом сохранять сравнимость по строке"{
            val translation = Translation(
                "rus",
                "eng",
                "собака",
                "dog"
            )
            val translationFact = TranslationFact(translation)
            // у нас никуда не делась возможность работать с ним типизировано
            translationFact.translation.resultLang shouldBe "eng"
            println(translationFact.toString())
            // но его идентичность все такая же не привязанная к его типу
            translationFact shouldBe "translation:::собака (rus) -> dog (eng)"
        }

        "Так как факты IWithIdentity их тоже можно матчить с паттернами"{
            val fact = TermFact("dog", Identity("me"))
            fact shouldBe "me:::term:::dog"
            fact shouldMatchPattern "**:::dog"
            fact shouldMatchPattern "**:::term:::dog"
            fact shouldMatchPattern "me:::**"
        }
    }
}
