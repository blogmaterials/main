package codes.spectrum.blog.semantics

import kotlin.random.Random

/*
Ниже примеры странной работы с условиями  на bool
типовая ситуация - есть набор простых и сложных признаков,
надо принять решение - делать что-то или нет, true или false
 */
/**
 * Делаю параметры рандомами, чтобы IDEA не пыталась соптимизировать константы
 */
/**
 * Дано что-то вроде этого
 */
fun randomBool() = listOf(true, false).random()
val count: Int = Random.nextInt()
val limit: Int = Random.nextInt()
val isRequired: Boolean = randomBool()
fun someExtendedCheck(): Boolean {
    Thread.sleep(1000)
    return listOf(true, false).random()
}

/**
 * Это то что нужно расчитать
 */
var result: Boolean? = null

fun `Варианты того что я видел в ревью`() {
    //1 - куча вложений и выходов

    /**
     * Понять эту семантику можно только так:
     * 1. я считаю что чем больше строк и блоков тем читаемее
     * 2. я не знаю что if в Kotlin это функция
     * 3. я умею писать if от одного условия и else, но не умею писать логические выражения
     */
    if (isRequired) {
        if (count < limit) {
            if (someExtendedCheck()) {
                result = true
            } else {
                result = false
            }
        } else {
            result = false
        }
    } else {
        result = false
    }
    //1 - тоже самое но хотя бы без множественных сайдов
    // тут только одно меняется "а я знаю что у Kotlin if - это функция!
    result = if (isRequired) {
        if (count < limit) {
            if (someExtendedCheck()) {
                true
            } else {
                false
            }
        } else {
            false
        }
    } else {
        false
    }
    //2 - "в kotlin же есть when!
    // тут новая семантика "паттерн матчинг и обход деревьев граничных условий это
    // круто и так код обрастает кучей функциональных "->", что еще круче
    result = when (isRequired) {
        true -> when {
            count < limit -> when {
                someExtendedCheck() -> true
                else -> false
            }
            else -> false
        }
        else -> false
    }
    //3 - типа изоляция длинного метода
    // семантика "я не доверяю компилятору и рантайму и отделю тяжелое условие от остального
    // выражение
    result = if (isRequired && (count < limit)) {
        someExtendedCheck()
    } else false
    //4 "в Kotlin" же есть takeIf и let и ?:
    // тут семантика "я настолько знаю все нюансы языка, что буду их использовать
    // всюду где синтаксис поволит мне это делать, я буду использовать
    // расширения, обработку null, условный let, тоже произолирую метод
    result = (isRequired && (count < limit)).takeIf { true }?.let { someExtendedCheck() } ?: false


    // но со времен C есть понятие ленивого вычисления булевых значений
    // и вообще тип Boolean системный, поэтому четкая семантика содержится в таком
    // выражении
    // " результат это коньюнкция условия на требование, на лимит и расширенной проверки"
    // " коньюнкты выстроены по возрастанию сложности вычисления для оптимизации
    result = isRequired && (count < limit) && someExtendedCheck()



}
