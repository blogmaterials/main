package codes.spectrum.blog.semantics

/**
 * Допустим вы где-то увидели такую функцию или Вам ее сказали реализовать,
 * не дав никаких вводны.
 * Тут это lateinit var чтобы я несколько вариантов реализации сделал,
 * так-то речь идет о
 * ```
 * /**
 * Представлоение исходной коллекции чего угодно как коллекции строк
 * */
 * fun Collection<*>.asStrings(): Collection<String>
 * ```
 */
lateinit var asStrings : Collection<*>.() -> Collection<String>

/*
Оказывается что без дополнительных вводных это можно достичь очень многими путями
завернем варианты в функцию так как в файле .kt ничего кроме определений быть не может
 */
fun `Варианты реализации asString()`() {
    /**
     * Первый вариант - мы поняли вопрос как обход предупреждения
     * `unchecked_cast` и что так как это `as*` то мы просто приводим
     * тип значнеия к `Collection<String>`
     */
    @Suppress("UNCHECKED_CAST")
    asStrings = {this as Collection<String>}

    /**
     * Тут схожее понимание, но это условно checked то есть все элементы проверены
     */
    @Suppress("UNCHECKED_CAST")
    asStrings = {
        if(all { it is String }) {
            this  as Collection<String>
        } else error("Не все строки!")
    }
    /**
     * Еще один вариант предварительной проверки на тип и всегда
     * возвращается новая коллекция, но при этом
     * теряется смысл `as` в названии
     */
    @Suppress("UNCHECKED_CAST")
    asStrings = { map {it as String} }

    /**
     * Это возврат НОВОЙ коллекции, в которой из исходной остались
     * только строки
     */
    asStrings = { filterIsInstance<String>() }

    /**
     * В этом варианте вернется исхдоная коллекция если
     * там строки, иначе вернется НОВАЯ коллекция в которой
     * все элементы вернутся в виде строк
     */
    @Suppress("UNCHECKED_CAST")
    asStrings = {
        if(all { i -> i is String }){
            this as Collection<String>
        }else map { it.toString() }
    }

    /**
     * Тут решается проблема null через их пропуск
     */
    asStrings = {  filterNotNull().map { it.toString() }  }

    /**
     * Тут решается проблема null через их явную вставку как "null"
     */
    asStrings = {  map { "$it" }  }


    /**
     * Тут null рассматриваются как пустые строки
     */
    asStrings = {  map { it?.toString() ?: "" }  }
}
