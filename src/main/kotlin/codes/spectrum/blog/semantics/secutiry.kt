package codes.spectrum.blog.semantics

import java.lang.Exception

/**
 * Свежий пример
 */
interface IUser {
    fun hasRole(role: String): Boolean
}

interface ICallContext {
    fun getCurrentUser(): IUser?
}

interface IReport
interface IRepository {
    fun getReport(uid: String): IReport
}

/**
 * Обеспечивает безопасный доступ к объектам данных
 */
class SecurityRepository(val db: IRepository) {
    fun getReport(uid: String, context: ICallContext): IReport {
        val report = db.getReport(uid)
        checkReport(report, context)
        return report
    }

    private fun checkReport(report: IReport, context: ICallContext) {
        context.getCurrentUser()?.also {
            if (it.hasRole("admin")) return
            throw Exception("Cannot access")
        }
    }

}
