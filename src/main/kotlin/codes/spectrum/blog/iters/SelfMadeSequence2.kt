package codes.spectrum.blog.iters

import kotlin.concurrent.thread
import kotlin.experimental.ExperimentalTypeInference

interface ILazyCollection<T> {
    fun yield(value: T)
}

@OptIn(ExperimentalTypeInference::class)
class SelfMadeSequence2<T>(
    @BuilderInference private val block: ILazyCollection<T>.() -> Unit
) : Iterable<T> {

    override fun iterator(): Iterator<T> {
        return SelfMadeSequenceIterator()
    }

    private enum class WorkerState {
        INIT,
        STARTING,
        STARTED,
        ERROR,
        FINISHED
    }

    private inner class SelfMadeSequenceIterator() : Iterator<T> {

        val lock = Object()
        var hasValue: Boolean = false
        var currentValue: T? = null

        private inner class SelfMadeSequenceScope() : ILazyCollection<T> {
            override fun yield(value: T) {
                currentValue = value
                hasValue = true
                lock.notifyAll()
                lock.wait()
            }
        }

        val singleStartLock = Object()
        private var error: Throwable? = null
        private var state: WorkerState = WorkerState.INIT
        lateinit var workerThread: Thread

        fun ensureStarted() {
            if (state != WorkerState.INIT) {
                return
            }
            synchronized(singleStartLock) {
                if (state == WorkerState.INIT) {
                    state = WorkerState.STARTING
                    workerThread = thread {
                        state = WorkerState.STARTED;
                        executeEmmiter()
                    }
                }
            }
        }

        private fun executeEmmiter() {
            synchronized(lock) {
                val scope = SelfMadeSequenceScope()
                try {
                    scope.block()
                    state = WorkerState.FINISHED
                } catch (e: Throwable) {
                    error = e
                    state = WorkerState.ERROR
                }finally {
                    lock.notifyAll()
                }
            }
        }

        private fun checkError(){
            if (state == WorkerState.ERROR) { throw error!! }
        }

        override fun hasNext(): Boolean {
            ensureStarted()
            synchronized(lock) {
                checkError()
                if (hasValue) {
                    return true
                } else {
                    lock.wait()
                    checkError()
                    return hasValue
                }
            }
        }

        override fun next(): T {
            ensureStarted()
            synchronized(lock) {
                if (hasNext()) {
                    hasValue = false
                    val value = currentValue
                    lock.notify()
                    return value as T
                } else {
                    throw error("Read from closed iterator")
                }
            }
        }

    }


}