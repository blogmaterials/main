package codes.spectrum.blog.mem

fun main() {
    var i: Int = 0
    for (s in sequence()) {
        i++
    }
    System.gc()
}

class ByteString(
    val data: CharArray = CharArray(100),
    var size: Int = 0,
    var s: String = "",
    var x: X = X()
)


class X

fun sequence() = sequence<ByteString> {
    val data = ByteString()
    var current = 0
    val prebuf: CharArray = CharArray(30)
    var presize: Int = 0
    var i = presize

    repeat(10000000) {
        data.size = 0
        data.data[0] = 's'
        data.data[1] = '_'
        current = it
        presize = 0
        while (current > 0) {
            val d = current % 10
            prebuf[presize++] = '0' + d
            current /= 10
        }
        i = presize
        while (i > 0) {
            data.data[2 + (presize - i)] = prebuf[i - 1]
            i--
        }
        data.size = 2 + presize
        //data.s = "s_$i"
        //data.x = X()
        yield(data)
    }
}