package codes.spectrum.blog.kotest


data class Toponym(
    val name: String,
    val type: ToponymType
)

interface IToponymParser {
    fun parse(addressElement: String) : Toponym
}


class ToponymParser : IToponymParser {
    override fun parse(addressElement: String): Toponym {
        TODO("Not yet implemented")
    }
}