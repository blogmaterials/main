package codes.spectrum.blog.kotest

/**
 * Тип топонима
 */
data class ToponymType(
    /**
     * Название
     */
    val name: String,
    /**
     * Варианты обозначений
     */
    val acronym: String
)

interface IToponymTypeRepostory {
    fun all(): List<ToponymType>
}


object Toponyms : IToponymTypeRepostory {
    private val items: List<ToponymType> by lazy {
        listOf(
            ToponymType("улица", "ул."),
            ToponymType("переулок", "пер."),
            ToponymType("бульвар", "б-р.")
        )
    }
    override fun all(): List<ToponymType> = items
}