package codes.spectrum.blog.architecture.kissorsolid.biz.solid.utils

interface IFioUtils {
    fun normalize(fio: String): String
}