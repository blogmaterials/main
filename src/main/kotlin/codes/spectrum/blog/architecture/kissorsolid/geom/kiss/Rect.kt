package codes.spectrum.blog.architecture.kissorsolid.geom.kiss

import java.lang.StrictMath.abs

/**
 * Прямоугольник, задается двумя сторонами [sideA] и [sideB],
 * если [sideB] не указать, она считается равной [sideA] и
 * это квадрат
 */
data class Rect(
    val sideA: Double,
    val sideB: Double = sideA
) : IShape {
    override fun perimeter(): Double = 2.0 * (sideA * sideB)
    override fun area(): Double = sideA * sideB

    /**
     * Признак квадрата
     */
    val isSquare = sideA == sideB

    companion object {
        fun createByPoints(oneCorner: Point, diagPoint: Point): Rect {
            require(oneCorner.dims == diagPoint.dims && oneCorner.dims == 2) {
                "Однозначно стороны можно определить по 2м точкам только в 2D"
            }
            return Rect(abs(oneCorner.x - diagPoint.x), abs(oneCorner.y - diagPoint.y))
        }
    }
}