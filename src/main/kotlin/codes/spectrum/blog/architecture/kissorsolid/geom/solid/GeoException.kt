package codes.spectrum.blog.architecture.kissorsolid.geom.solid

import kotlin.reflect.KClass

sealed class GeoException(message: String, cause: Throwable?) : Throwable(message, cause) {
    class TooGenericNumberTypeForActualValue(val clazz: KClass<out Number>) :
        GeoException("Too generic type `$clazz` fro actual value", null)

    class NotSupportedNumberType(val clazz: KClass<out Number>) :
        GeoException("Number type `$clazz` is not supported for now", null)

    class NotMatchedRequirements(message: String, cause: IllegalArgumentException) :
        GeoException(message, cause)

    class NotExistedDimension(dimension: Dimension) :
        GeoException("No value for dimension `$dimension`", null)
}