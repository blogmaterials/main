package codes.spectrum.blog.architecture.kissorsolid.biz.solid.adapters

import codes.spectrum.blog.architecture.kissorsolid.biz.solid.NormalData
import codes.spectrum.blog.architecture.kissorsolid.biz.solid.VendorData
import codes.spectrum.blog.architecture.kissorsolid.biz.solid.utils.FioUtils
import codes.spectrum.blog.architecture.kissorsolid.biz.solid.utils.IFioUtils
import codes.spectrum.blog.architecture.kissorsolid.biz.solid.utils.IRegionsUtils
import codes.spectrum.blog.architecture.kissorsolid.biz.solid.utils.RegionsUtils
import java.time.LocalDate

internal class VendorToNormalConverterImpl(
    private val fioUtils: IFioUtils = FioUtils.Default,
    private val regionsUtils: IRegionsUtils = RegionsUtils.Default
) : IVendorToNormalConverter {
    override fun convert(source: VendorData): NormalData {
        val fio = fioUtils.normalize(source.fio ?: "")
        val age = source.age ?: -1
        val actualityDate = source.recordDate?.let { it.toLocalDate() } ?: LocalDate.MIN
        val regions = source.region_codes?.let { regionsUtils.extractRegionCodes(it) } ?: emptySet()
        return NormalData(fio, age, actualityDate, regions)
    }

    companion object {
        val Default by lazy { VendorToNormalConverterImpl() }
        val Descriptor by lazy {
            ModelConverterDescriptor(
                VendorData::class,
                NormalData::class
            ) { Default }
        }
    }
}