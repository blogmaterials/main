package codes.spectrum.blog.architecture.kissorsolid.biz.solid

import java.time.LocalDateTime

data class VendorData(
    @SourceName("familiya_i_imya")
    var fio: String? = null,
    @SourceName("vozrast")
    var age: Int? = null,
    var recordDate: LocalDateTime? = null,
    var region_codes: String? = null
): ISourceModelMarker