package codes.spectrum.blog.architecture.kissorsolid.geom.solid

sealed class Dimension(val number: Int) {
    object NULL : Dimension(0)
    object X : Dimension(1)
    object Y : Dimension(2)
    object Z : Dimension(3)
    private class NotEuqlidious(number: Int) : Dimension(number)
    companion object {
        fun getForNumber(number: Int): Dimension = when (number) {
            0 -> NULL
            1 -> X
            2 -> Y
            3 -> Z
            else -> NotEuqlidious(number)
        }
    }
}