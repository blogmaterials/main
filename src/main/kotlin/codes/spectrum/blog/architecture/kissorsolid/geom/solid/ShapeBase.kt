package codes.spectrum.blog.architecture.kissorsolid.geom.solid

import kotlin.reflect.KClass

/**
 * Абстрактная фигура, фиксирует факт, что [len] равен [perimeter],
 * также мы замыкаем расчеты на [GeoMath]
 */
abstract class ShapeBase<D : Number>(protected val points: List<Point<D>>) : IShape<D> {
    // TODO: вообще надо избавиться от всех этих методов
    // сделать IShape тоже маркерным и все вынести в расширения
    // GeoMath
    override fun len(): D = perimeter()
    override fun perimeter(): D = GeoMath.CalculatePerimeter(this)
    override fun area(): D = GeoMath.CalculateArea(this)
    override fun actualNumberClass(): KClass<out D> {
        return (points.firstOrNull()?.actualNumberClass() ?: Number::class) as KClass<out D>
    }

    init {
        require(points.size >= 2 && points.all { !it.isNull } && points.all { it.x::class == points.first().x::class }) {
            "Любая фигура может быть определна минимум 2 точками одного пространства"
        }
    }
}

