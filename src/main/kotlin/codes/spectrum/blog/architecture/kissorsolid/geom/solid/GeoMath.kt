package codes.spectrum.blog.architecture.kissorsolid.geom.solid

import java.lang.IllegalArgumentException
import kotlin.math.roundToInt
import kotlin.math.roundToLong
import kotlin.reflect.KClass

/**
 * Обеспечим унифицированный вариант вычислений для разных типов чисел.
 * Собственно тут концентрируется весь математический аппарат
 */
object GeoMath {

    private fun <D : Number> BackCast(v: Double, ret: D, strategy: RoundStrategy = RoundStrategy.CLOSE): D {
        return when (ret) {
            is Double -> v
            is Int -> if (strategy == RoundStrategy.CLOSE) v.roundToInt() else v.toInt()
            is Short -> if (strategy == RoundStrategy.CLOSE) v.roundToInt().toShort() else v.toInt().toShort()
            is Long -> if (strategy == RoundStrategy.CLOSE) v.roundToLong().toShort() else v.toLong()
            else -> throw GeoException.NotSupportedNumberType(ret::class as KClass<out Number>)
        } as D
    }

    fun <D : Number> Mod(a: D, b: D, strategy: RoundStrategy = RoundStrategy.CLOSE): D {
        return BackCast(Math.abs(b.toDouble() - a.toDouble()), a, strategy)
    }

    fun <D : Number> Sqr(a: D, strategy: RoundStrategy = RoundStrategy.CLOSE): D {
        return BackCast(a.toDouble().let { it * it }, a, strategy)
    }

    enum class RoundStrategy {
        FLOOR,
        CLOSE
    }

    /**
     * Вычисление расстояния между точками
     */
    fun <D : Number> Distance(a: Point<D>, b: Point<D>, strategy: RoundStrategy = RoundStrategy.CLOSE): D {
        try {
            require(!a.isNull && !b.isNull) {
                "Точки не должны быть нуллевого размера"
            }
            require(a.dimensions == b.dimensions) {
                "Точки должны быть в одинаковых пространствах"
            }
            require(a.x::class == b.x::class) {
                "Точки должны иметь один тип значений"
            }
        } catch (e: IllegalArgumentException) {
            throw GeoException.NotMatchedRequirements("Point distance $a, $b", e)
        }
        /**
         * Получим дистанции по каждой координате
         */
        val offsets = (1..a.dimensions).map { dim ->
            Mod(b[dim], a[dim])
        }

        /**
         * Получим сумму квадратов
         */
        val squareSum = offsets.fold(0.0) { acc, d -> acc + Sqr(d).toDouble() }

        /**
         * Получим вещественное значение расстояния
         */
        val resultDouble = Math.sqrt(squareSum)

        return BackCast(resultDouble, a.x, strategy)
    }

    fun <D : Number, S : ShapeBase<D>> CalculatePerimeter(
        shape: S,
        strategy: RoundStrategy = RoundStrategy.CLOSE
    ): D {
        return when {
            shape is Circle<*> -> BackCast(
                2.0 * Math.PI * Distance(
                    shape.centerPoint as Point<D>,
                    shape.contourPoint as Point<D>,
                    strategy
                ).toDouble(),
                shape.contourPoint.x,
                strategy
            )
            else -> TODO()
        }
    }

    fun <D : Number, S : ShapeBase<D>> CalculateArea(shape: S, strategy: RoundStrategy = RoundStrategy.CLOSE): D {
        return when {
            shape is Circle<*> -> BackCast(
                Math.PI * Distance(
                    shape.centerPoint as Point<D>,
                    shape.contourPoint as Point<D>,
                ).toDouble().let { it * it },
                shape.contourPoint.x,
                strategy
            )
            else -> TODO()
        }
    }
}

fun <D : Number> Point<D>.distanceTo(other: Point<D>) = GeoMath.Distance(this, other)