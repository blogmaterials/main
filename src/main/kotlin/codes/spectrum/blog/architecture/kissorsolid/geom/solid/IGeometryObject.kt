package codes.spectrum.blog.architecture.kissorsolid.geom.solid

import kotlin.reflect.KClass

/**
 * Общий маркерный интерфейс для всего, что связано с нашим пакетом
 * для того чтобы обеспечить явную легко проверяемую изоляцию
 * @param D - размерность, для которой определены все значения,
 * чтобы обеспечить как целочисленную дискретную геометрию,
 * так и вещественную и при необходимости комплексную
 */
interface IGeometryObject<D : Number> {
    fun actualNumberClass(): KClass<out D>
}

