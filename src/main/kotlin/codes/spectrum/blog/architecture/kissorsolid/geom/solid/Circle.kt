package codes.spectrum.blog.architecture.kissorsolid.geom.solid

class Circle<D : Number>(
    val centerPoint: Point<D>,
    val contourPoint: Point<D>
) : ShapeBase<D>(points = listOf(centerPoint, contourPoint)) {
    val radius by lazy { centerPoint.distanceTo(contourPoint) }
    companion object{
        fun  createForRadius(d: Double): Circle<Double> {
            return Circle(Point(0.0, 0.0), Point(0.0, d))
        }
    }
}