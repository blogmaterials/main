package codes.spectrum.blog.architecture.kissorsolid.biz.solid.adapters

import codes.spectrum.blog.architecture.kissorsolid.biz.solid.NormalData
import codes.spectrum.blog.architecture.kissorsolid.biz.solid.VendorData

interface IVendorToNormalConverter: IModelConverter<VendorData, NormalData>

