package codes.spectrum.blog.architecture.kissorsolid.biz.solid.adapters

import codes.spectrum.blog.architecture.kissorsolid.biz.solid.IResultModelMarker
import codes.spectrum.blog.architecture.kissorsolid.biz.solid.ISourceModelMarker
import kotlin.reflect.KClass

interface IModelConverterFactory {
    fun <IN : ISourceModelMarker, OUT : IResultModelMarker> getConverter(from: KClass<IN>, to: KClass<OUT>):
            IModelConverter<IN, OUT>
}