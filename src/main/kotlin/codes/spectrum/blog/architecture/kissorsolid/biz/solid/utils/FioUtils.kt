package codes.spectrum.blog.architecture.kissorsolid.biz.solid.utils

class FioUtils(private val splitter: Regex = """\s+""".toRegex()): IFioUtils {
    override fun normalize(fio: String): String {
        return fio.trim().toUpperCase().replace(splitter, " ")
    }

    companion object {
        val Default by lazy { FioUtils() }
    }
}