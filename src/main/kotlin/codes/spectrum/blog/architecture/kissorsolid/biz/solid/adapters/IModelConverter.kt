package codes.spectrum.blog.architecture.kissorsolid.biz.solid.adapters

import codes.spectrum.blog.architecture.kissorsolid.biz.solid.IResultModelMarker
import codes.spectrum.blog.architecture.kissorsolid.biz.solid.ISourceModelMarker

interface IModelConverter<IN : ISourceModelMarker, OUT : IResultModelMarker> {
    fun convert(source: IN): OUT
}


