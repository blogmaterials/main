package codes.spectrum.blog.architecture.kissorsolid.biz.solid.adapters

import codes.spectrum.blog.architecture.kissorsolid.biz.solid.IResultModelMarker
import codes.spectrum.blog.architecture.kissorsolid.biz.solid.ISourceModelMarker
import kotlin.reflect.KClass

data class ModelConverterDescriptor<IN : ISourceModelMarker, OUT : IResultModelMarker>(
    val srcClazz: KClass<IN>,
    val outClazz: KClass<OUT>,
    val provider: () -> IModelConverter<IN, OUT>
)