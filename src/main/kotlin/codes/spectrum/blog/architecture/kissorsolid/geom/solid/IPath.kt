package codes.spectrum.blog.architecture.kissorsolid.geom.solid

/**
 * Контур - вариант геометрического объекта
 */
interface IPath<D:Number> : IGeometryObject<D> {
    /**
     * Общая длина пути
     */
    fun len() : D
}