package codes.spectrum.blog.architecture.kissorsolid.biz.kiss

import codes.spectrum.blog.architecture.kissorsolid.biz.solid.NormalData
import java.sql.Connection
import java.time.LocalDate
import java.time.LocalDateTime

internal data class VendorData(
    var familiya_i_imya: String? = null,
    var vozrast: Int? = null,
    var recordDate: LocalDateTime? = null,
    var region_codes: String? = null
) {
    fun toNormal(): NormalData {
        val fio = NormalizeUtils.normalFio(familiya_i_imya ?: "")
        val age = vozrast ?: -1
        val actualityDate = recordDate?.let { it.toLocalDate() } ?: LocalDate.MIN
        val regions = region_codes?.let { NormalizeUtils.extractNumbers(it).toSet() } ?: emptySet()
        return NormalData(fio, age, actualityDate, regions)
    }
}