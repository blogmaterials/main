package codes.spectrum.blog.architecture.kissorsolid.geom.solid

/**
 * Фигура - замкнутрый вариант контура,
 * для которого определны периметр и площадь
 */
interface IShape<D:Number> : IPath<D> {
    @Deprecated("Фигуры сами не должны знать своих производных значений")
    fun perimeter() : D
    fun area() : D
}