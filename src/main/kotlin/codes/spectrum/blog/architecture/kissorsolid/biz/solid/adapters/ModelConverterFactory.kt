package codes.spectrum.blog.architecture.kissorsolid.biz.solid.adapters

import codes.spectrum.blog.architecture.kissorsolid.biz.solid.IResultModelMarker
import codes.spectrum.blog.architecture.kissorsolid.biz.solid.ISourceModelMarker
import kotlin.reflect.KClass

class ModelConverterFactory(private val converters: List<ModelConverterDescriptor<*, *>>) :
    IModelConverterFactory {
    override fun <IN : ISourceModelMarker, OUT : IResultModelMarker> getConverter(
        from: KClass<IN>,
        to: KClass<OUT>
    ): IModelConverter<IN, OUT> {
        return converters.first {
            it.srcClazz == from && it.outClazz == to
        }.provider() as IModelConverter<IN, OUT>
    }

    companion object {
        val Default by lazy {
            ModelConverterFactory(
                listOf(
                    VendorToNormalConverterImpl.Descriptor
                )
            )
        }
    }
}

inline fun <reified OUT: IResultModelMarker> ISourceModelMarker.convertTo() =
    ModelConverterFactory.Default.getConverter(this::class as KClass<ISourceModelMarker>, OUT::class)
        .convert(this)