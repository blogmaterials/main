package codes.spectrum.blog.architecture.kissorsolid.biz.solid.utils

interface IRegionsUtils {
    fun extractRegionCodes(rawString: String): Set<Int>
}