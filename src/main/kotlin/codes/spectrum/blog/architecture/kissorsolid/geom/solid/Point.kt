package codes.spectrum.blog.architecture.kissorsolid.geom.solid

import kotlin.reflect.KClass

/**
 * Минимальный геометрический объект - точка в N-arnom пространстве
 */
data class Point<D : Number>(
    private val coords: List<D> = emptyList()
): IGeometryObject<D> {
    constructor(first: D, vararg others: D) : this(listOf(first, *others))

    /**
     * Признак нулевой отсутствующей точки
     */
    val isNull = coords.isEmpty()

    /**
     * Количество измерений
     */
    val dimensions = coords.size

    /**
     * Координата X
     */
    val x: D by lazy { get(Dimension.X) }

    /**
     * Координата Y
     */
    val y: D by lazy { get(Dimension.Y) }

    /**
     * Координата Z
     */
    val z: D by lazy { get(Dimension.Z) }

    /**
     * Координата в произвольном измерении
     */
    operator fun get(dimension: Dimension): D =
        if (dimension != Dimension.NULL && coords.size > dimension.number - 1)
            coords[dimension.number - 1] else
            throw GeoException.NotExistedDimension(dimension)

    operator fun get(number: Int): D = get(Dimension.getForNumber(number))
    override fun actualNumberClass(): KClass<out D> = (if(isNull) Number::class else x::class) as KClass<out D>
}