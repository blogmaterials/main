package codes.spectrum.blog.architecture.kissorsolid.geom.kiss

/**
 * Круг, задаваемый через радиус [radius]
 */
data class Circle(
    val radius: Double
) : IShape {
    constructor(radius: Number):this(radius.toDouble())
    override fun perimeter(): Double = 2.0 * Math.PI * radius
    override fun area(): Double = Math.PI * radius * radius

    companion object {
        /**
         * Во многих случаях нам известен диаметр, а не радиус
         */
        fun createByDiameter(diameter: Double): Circle {
            return Circle(diameter / 2.0)
        }

        fun createByPoints(center:Point, atContour: Point) : Circle {
            return Circle(center.ditanceTo(atContour))
        }
    }
}

