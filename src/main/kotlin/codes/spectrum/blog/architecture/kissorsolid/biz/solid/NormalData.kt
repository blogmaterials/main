package codes.spectrum.blog.architecture.kissorsolid.biz.solid

import java.time.LocalDate

data class NormalData(
    val fio: String = "",
    val age: Int = -1,
    val actualityDate: LocalDate = LocalDate.MIN,
    val regions: Set<Int> = emptySet()
) : IResultModelMarker