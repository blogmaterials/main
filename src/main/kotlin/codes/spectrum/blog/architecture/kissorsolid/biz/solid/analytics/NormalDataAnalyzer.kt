package codes.spectrum.blog.architecture.kissorsolid.biz.solid.analytics

import codes.spectrum.blog.architecture.kissorsolid.biz.solid.NormalData
import java.time.LocalDate

class NormalDataAnalyzer(val normalData: NormalData) {
    val isFioDefined get() = normalData.fio.isNotBlank()
    val isActualityDefined get() = normalData.actualityDate != LocalDate.MIN
    val isAgeDefined get() = normalData.age > -1
    val minBirthYear get() = if (isAgeDefined && isActualityDefined) normalData.actualityDate.year - normalData.age - 1 else -1
    val maxBirthYear get() = if (isAgeDefined && isActualityDefined) normalData.actualityDate.year - normalData.age else -1
}