package codes.spectrum.blog.architecture.kissorsolid.biz.kiss

object NormalizeUtils {
    // "  Иванов  Иван \n ИвановИч " -> "ИВАНОВ ИВАН ИВАНОВИЧ"
    fun normalFio(rawFio: String): String {
        return rawFio.trim().toUpperCase().replace("""\s+""".toRegex(), " ")
    }

    // извлекает коды из всяких строк типа "1 3 4", "1, 55, 66", "МОсква:95, Свердловск: 66"
    fun extractNumbers(rawString: String): List<Int> {
        return rawString.split("""\D+""".toRegex()).map { it.toIntOrNull() }.filterNotNull()
    }
}