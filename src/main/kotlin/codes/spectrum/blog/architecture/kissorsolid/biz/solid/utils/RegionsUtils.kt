package codes.spectrum.blog.architecture.kissorsolid.biz.solid.utils


class RegionsUtils: IRegionsUtils {
    override fun extractRegionCodes(rawString: String): Set<Int> {
        return rawString.split("""\D+""".toRegex()).map { it.toIntOrNull() }.filterNotNull().toSet()
    }
    companion object {
        val Default by lazy { RegionsUtils() }
    }
}

