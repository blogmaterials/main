package codes.spectrum.blog.architecture.kissorsolid.biz.kiss

import java.time.LocalDate

data class NormalData(
    val fio: String = "",
    val age: Int = -1,
    val actualityDate: LocalDate = LocalDate.MIN,
    val regions: Set<Int> = emptySet()
) {
    val isFioDefined get() = fio.isNotBlank()
    val isActualityDefined get() = actualityDate != LocalDate.MIN
    val isAgeDefined get() = age > -1
    val minBirthYear get() = if (isAgeDefined && isActualityDefined) actualityDate.year - age - 1 else -1
    val maxBirthYear get() = if (isAgeDefined && isActualityDefined) actualityDate.year - age else -1
}