package codes.spectrum.blog.architecture.kissorsolid.biz.solid

/**
 * Чтобы не портить свой код помечаем
 * какие были исходные поля, но именуем нормально
 */
annotation class SourceName(val name: String)