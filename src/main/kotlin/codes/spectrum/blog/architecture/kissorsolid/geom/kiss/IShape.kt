package codes.spectrum.blog.architecture.kissorsolid.geom.kiss

interface IShape {
    fun perimeter(): Double
    fun area(): Double
}