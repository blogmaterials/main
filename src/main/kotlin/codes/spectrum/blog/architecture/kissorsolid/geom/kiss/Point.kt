package codes.spectrum.blog.architecture.kissorsolid.geom.kiss

import kotlin.math.sqrt

data class Point(
    private val coords: List<Double> = emptyList()
) {
    constructor(vararg coords: Double) : this(coords.toList())
    constructor(vararg coords: Number) : this(coords.map { it.toDouble() }.toList())

    operator fun get(dim: Int) = coords.elementAtOrNull(dim) ?: Double.NaN
    val dims get() = coords.size
    val isDefined get() = dims > 0 && coords.all { it.isFinite() }
    val x by lazy { get(0) }
    val y by lazy { get(1) }
    val z by lazy { get(2) }

    fun ditanceTo(other: Point): Double {
        require(this.dims == other.dims) {
            "Разная размерность точек, this: ${this.dims} и other: ${other.dims}"
        }
        require(this.isDefined && other.isDefined) {
            "Обе точки должны быть полностью определены, this: ${coords.joinToString()}, other: ${other.coords.joinToString()}"
        }
        return this.coords.zip(other.coords).map { coordPair ->
            (coordPair.first - coordPair.second).let { it * it }
        }.sum().let { sqrt(it) }
    }
}