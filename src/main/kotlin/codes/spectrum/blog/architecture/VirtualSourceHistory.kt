package codes.spectrum.blog.architecture

class `SpectrumData поколение 0 - фиксированный набор данных` {

    class Report(val data: String)

    object Site {
        fun getReport(query: String): Report {
            val data = Processing.getReportData(query)
            return Report(data)
        }
    }

    object Processing {
        fun getReportData(query: String): String {
            return "${sourceOne(query)}; ${sourceTwo(query)}"
        }
        fun sourceOne(query: String): String = "${query}:1"
        fun sourceTwo(query: String): String = "${query}:2"
    }
}

class `SpectrumData поколение 1 - клиенто-ориентированные отчеты` {

    class Report(val data: String, state: String = "OK", error: Throwable? = null)

    object Site {
        fun getReport(query: String, client: String): Report {
            return B2B.getReport(query, client)
        }
    }

    object B2B {
        val clientDb = mapOf(
            "client1" to "full_report",
            "client2" to "short_report",
            "client3" to "short_report",
            "client4" to "full_report_only_one",
            "client5" to "short_report_one_if_error_two"
        )

        fun getReport(query: String, client: String): Report {
            return try {
                val report_type = clientDb[client]!!
                val data = Processing.getReportData(query, report_type)
                Report(data)
            } catch (e: Throwable) {
                Report("", "Error", e)
            }
        }
    }

    object Processing {
        fun getReportData(query: String, report_type: String): String {
            return when (report_type) {
                "full_report" -> "${sourceOneLong(query)}; ${sourceTwo(query)}"
                "short_report" -> "${sourceOneShort(query)}; ${sourceTwo(query)}"
                "full_report_only_one" -> sourceOneLong(query)
                "short_report_one_if_error_two" -> {
                    try {
                        sourceOneShort(query)
                    } catch (e: Throwable) {
                        sourceTwo(query)
                    }
                }
                else -> error("Неизвестный отчет")
            }
        }

        fun sourceOneShort(query: String): String = "${query}:1"
        fun sourceOneLong(query: String): String = "${query}:1-hello-my-world"
        fun sourceTwo(query: String): String = "${query}:2"
    }
}

class `SpectrumData поколение 2 - появление типов отчета, отделение источников, коды источников` {

    class ReportType(val sources: List<String>)
    class Report(
        val data: String,
        state: String = "OK",
        error: Throwable? = null
    )

    object Site {
        fun getReport(query: String, client: String, report_type: String): Report {
            return B2B.getReport(query, client, report_type)
        }
    }

    object B2B {
        val clientDb = mapOf(
            "client1" to mapOf(
                "full" to ReportType(listOf("sourceOneFull", "sourceTwo")),
                "short" to ReportType(listOf("sourceOneShort", "sourceTwo")),
            ),
            "client2" to mapOf(
                "one" to ReportType(listOf("sourceOneFull")),
                "one_or_two" to ReportType(listOf("oneOrTwo")),
            )
        )

        fun getReport(query: String, client: String, report_type: String): Report {
            return try {
                val report_type = clientDb[client]!![report_type]!!
                val data = Processing.getReportData(query, report_type)
                Report(data)
            } catch (e: Throwable) {
                Report("", "Error", e)
            }
        }
    }

    object Processing {
        fun getReportData(query: String, report_type: ReportType): String {
            val results = mutableListOf<String>()
            for (source in report_type.sources) {
                when (source) {
                    "oneOrTwo" -> results.add(
                        try {
                            Sources.getData("sourceOneShort", query)
                        } catch (e: Throwable) {
                            Sources.getData("sourceTwo", query)
                        }
                    )
                    else -> results.add(Sources.getData(source, query))
                }
            }
            return results.joinToString("; ")
        }
    }

    object Sources {
        interface ISource {
            fun get(query: String): String
        }

        object SourceOneFull : ISource {
            override fun get(query: String): String {
                return "${query}:1"
            }
        }

        object SourceOneShort : ISource {
            override fun get(query: String): String {
                return "${query}:1-hello-my-world"
            }
        }

        object SourceTwo : ISource {
            override fun get(query: String): String {
                return "${query}:2"
            }
        }

        val sourceMap = mapOf<String, ISource>(
            "sourceOneFull" to SourceOneFull,
            "sourceOneShort" to SourceOneShort,
            "sourceTwo" to SourceTwo
        )

        fun getData(source: String, query: String): String {
            return sourceMap[source]!!.get(query)
        }
    }
}

class `SpectrumData поколение 3 - упрощение логики процессинга, полная изоляция источников` {

    // тут нет изменений
    class ReportType(val sources: List<String>)
    class Report(
        val data: String,
        state: String = "OK",
        error: Throwable? = null
    )

    // и тут нет изменений
    object Site {
        fun getReport(query: String, client: String, report_type: String): Report {
            return B2B.getReport(query, client, report_type)
        }
    }

    // class VirtualSource

    // да и тут нет
    object B2B {
        val clientDb = mapOf(
            "client1" to mapOf(
                "full" to ReportType(listOf("sourceOneFull", "sourceTwo")),
                "short" to ReportType(listOf("sourceOneShort", "sourceTwo")),
            ),
            "client2" to mapOf(
                "one" to ReportType(listOf("sourceOneFull")),
                "one_or_two" to ReportType(listOf("oneOrTwo")),
            )
        )

        fun getReport(query: String, client: String, report_type: String): Report {
            return try {
                val report_type = clientDb[client]!![report_type]!!
                val data = Processing.getReportData(query, report_type)
                Report(data)
            } catch (e: Throwable) {
                Report("", "Error", e)
            }
        }
    }

    // упрощение здесь
    object Processing {
        fun getReportData(query: String, report_type: ReportType): String {
            return report_type.sources
                .map { Sources.getData(it, query) }
                .joinToString("; ")
        }
    }

    // существенная переработка здесь
    object Sources {
        interface ISource {
            // источник тоже имеет доступ к коду с которым вызван
            fun get(query: String, sourceCode: String): String
            // и сообщает о том, какие коды он поддерживает
            val supportedCodes : List<String>
        }

        object SourceOne : ISource {
            override val supportedCodes: List<String> = listOf("sourceOneFull","sourceOneShort","oneOrTwo")
            override fun get(query: String, sourceCode: String): String {
                return when(sourceCode){
                    "sourceOneFull" -> "${query}:1-hello-my-world"
                    "sourceOneShort" -> "${query}:1"
                    "oneOrTwo" -> try{
                        get(query, "sourceOneShort")
                    }catch (e: Throwable){
                        SourceTwo.get(query,"sourceTwo")
                    }
                    else -> error("Неизвестный код")
                }
            }
        }

        object SourceTwo : ISource {
            override val supportedCodes: List<String> = listOf("sourceTwo")
            override fun get(query: String, sourceCode: String): String {
                return "${query}:2" // у него нет никаких вариантов, можем игнорировать
            }
        }

        // тут достаточно списков
        val sources = listOf(SourceOne, SourceTwo)

        // тут косвенная а не прямая логика мапинга
        fun getData(source: String, query: String): String {
            return sources
                .first{it.supportedCodes.contains(source)}
                .get(query, source)
        }
    }
}


// И вот тут собственно и началось расхождение в понятиях и потребовался
// единый язык для обозначения этих сущностей и разделения реализации
// и использования
