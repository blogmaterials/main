package codes.spectrum.blog.architecture.determinism.iter_02

/**
 * Хотя это и главный сервис он уже не знает
 * ничего насчет того что он выводит и куда
 * просто посредник
 */
class Greater(val config: Config, val logger: Logger) {
    fun greet() {
        logger.log(config.message)
    }
}