package codes.spectrum.blog.architecture.determinism.iter_02

/**
 * Конфиг не знает ничего кроме того, что ему надо прочитать
 * два параметра и что дефолты это hello world и STDOUT
 * по сути это вариант карго культа - вроде этот конфиг и выражает
 * наш контракт, но только он никак его сам не выполняет
 * и не понимает его смысл
 */
class Config(args: Array<String>) {
    private val argmap = args.toList().chunked(2).map { it.elementAt(0).trim('-') to it.elementAt(1) }.toMap()
    val message by lazy { argmap["message"] ?: "hello world" }
    val out by lazy { argmap["out"] ?: "STDOUT" }
}