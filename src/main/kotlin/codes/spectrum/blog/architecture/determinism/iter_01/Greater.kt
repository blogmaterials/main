package codes.spectrum.blog.architecture.determinism.iter_01

class Greater {
    fun greet() {
        // класс все еще уверен что выводит hello world в консоль
        println("hello world")
    }
}