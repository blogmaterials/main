package codes.spectrum.blog.architecture.determinism.iter_02

import kotlin.system.measureTimeMillis

/**
 * точка входа ничего не знает ни про hello world ни про вывод в консоль
 * знает только что ей надо выполнить приложение передав ему свои параметры
 *
 * самое интересное, что теперь мы вполне соответствуем ТЗ и еще и с БОНУСАМИ
 */
fun main(args: Array<String>) {
    println("Our company best product!")
    println("executing...")
    val time = measureTimeMillis {
        App(args).execute()
    }
    println("execution time: $time ms")
}

