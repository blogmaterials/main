package codes.spectrum.blog.architecture.determinism.iter_02

/**
 * Приложение тоже на самом деле не знает что будет происходить
 * знает только что основной сервис - это greater,
 * и что для его работы нужен логер и конфиг
 */
class App(val args:Array<String>) {
    val config by lazy { Config(args) }
    val logger by lazy { Logger(config) }
    val greater by lazy { Greater(config, logger) }
    fun execute() {
        greater.greet()
    }
}