package codes.spectrum.blog.architecture.determinism.iter_01

fun main(){
    // что думает программист "я использую Greater, который печатает hello world в консоль
    // факт - я даю сигнал greet не контроллируемой мною сущности , получаемой вызовом Greater()
    Greater().greet()
}