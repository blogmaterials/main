package codes.spectrum.blog.architecture.determinism.iter_02

import java.io.File

/**
 * Ничего не знает про то что он будет писать
 * и пишет не только в консоль
 */
class Logger(val config: Config) {
    fun log(message: String) {
        if(config.out == "STDOUT") {
            println(message)
        }else{
            File(config.out).also {
                it.parentFile?.mkdirs()
                it.appendText(message + "\n")
            }
        }
    }
}