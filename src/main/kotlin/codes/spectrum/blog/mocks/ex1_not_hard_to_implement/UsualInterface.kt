package codes.spectrum.blog.mocks

/**
 * Данная группа приме
 *
 */

/**
 * Некий обычный интерфейс
 */
interface ISomeDbIface {
    fun find(name: String): String
    fun execute(name: String, data: Int = 0): Boolean
}

interface ISomeDbClient {
    fun getrealname() : String
    fun execute(data: Int = 3): Boolean
}

class WorkingUsualInterfaceClient(private val db: ISomeDbIface) : ISomeDbClient {
    private val name = "somename"
    private val resolvedName by lazy { db.find(name) } // кэширует значение из базы
    override fun getrealname() = resolvedName
    override fun execute(data: Int) = db.execute(getrealname(), data) // использует корректное имя
}

class BugUsualInterfaceClient(private val db: ISomeDbIface) : ISomeDbClient {
    private val name = "somename"
    override fun getrealname() = db.find(name) // будет перечитывать каждый раз
    override fun execute(data: Int) = db.execute(name, data) // использует исходное,а не зарезольвенное имя
}


class PartiallyBugClient(private val db: ISomeDbIface) : ISomeDbClient {
    private val name = "somename"
    override fun getrealname() = db.find(name) // будет перечитывать каждый раз
    override fun execute(data: Int) = db.execute(getrealname(), data)
}
