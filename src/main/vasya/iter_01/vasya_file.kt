package codes.spectrum.blog.architecture.determinism.iter_01

import codes.spectrum.blog.architecture.determinism.iter_02.Greater

/**
 * Это улучшенная версия принтлайна
 * так как можно прямо указать что писать в STDERR
 */
fun println(s: String, error: Boolean = false) {
    if(error){
        System.err.println(s)
    }else {
        System.out.println(s)
    }
}
