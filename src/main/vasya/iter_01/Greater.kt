package codes.spectrum.blog.architecture.determinism.iter_01

/**
 * vasya - нужно всегда сегрегировать интерфепйсы solId
 */
interface IGreater {
    fun greet()
}

/**
 * vasya - Нужно скрывать реализацию
 */
private class GreaterImpl: IGreater {
    override fun greet() {
        // класс все еще уверен что выводит hello world в консоль
        println("hello world")
    }
}

/**
 * vasys - Для совместимости оставим конструктор
 */
fun Greater(): IGreater = GreaterImpl()