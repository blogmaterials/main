package codes.spectrum.blog.architecture.determinism.iter_02

/**
 * vasya - нужно всегда сегрегировать интерфепйсы solId
 * ну и раз один метод, то это fun interface
 */
fun interface IGreater {
    fun greet()
}

/**
 * vasys - Для совместимости оставим конструктор
 */
fun Greater(config: Config? = null, logger: Logger? = null): IGreater = IGreater {
    (config?.message ?: "hello world").also{logger?.log(it) ?: println(it)}
}