package codes.spectrum.blog.architecture.determinism.iter_02

import java.io.File
import java.io.PrintStream

/**
 * Конфиг не знает ничего кроме того, что ему надо прочитать
 * два параметра и что дефолты это hello world и STDOUT
 * по сути это вариант карго культа - вроде этот конфиг и выражает
 * наш контракт, но только он никак его сам не выполняет
 * и не понимает его смысл
 */
class Config(args: Array<String>) {
    private val argmap = args.toList().chunked(2).map { it.elementAt(0).trim('-') to it.elementAt(1) }.toMap()
    val message by lazy { argmap["message"] ?: "hello world" }
    // vasya - все выполняем требование про STDOUT, сделаем редиректом
    val out = "STDOUT"

    init {
        // vasya на связи - собственно зачем делать задвоение имени файла, если
        // передали файл, значит вывод надо вывести на него, а для совместимости оставим, что он STDOUT
        val outfromargs = argmap["out"] ?: "STDOUT"
        if (outfromargs != "STDOUT") {
            val printer = PrintStream(File(outfromargs))
            System.setOut(printer)
        }
    }
}